<?php
/**
 * Layout panel configuration example.
 *
 * @var array
 */
$settings = array(

	'show-btn' => true,
	'btn-link' => 'https://www.templatemonster.com/cart.php?add=69580&price_variant=regular',

	'show-filters' => false,
	'filters'      => array(
		'skins'   => esc_html__('Skins', 'storycle'),
		'headers' => esc_html__('Headers', 'storycle'),
		'footers' => esc_html__('Footers', 'storycle'),
	),

	'primary-color' => '#377EFF',
	'scroll-color'  => '#558DD9',

	'items_get_from' => 'https://raw.githubusercontent.com/ZemezLab/zemez-themes-panel/master/storycle/items.json',

);
