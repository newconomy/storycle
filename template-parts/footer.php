<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Storycle
 */
?>
<div class="footer-container">
	<div class="container">
		<div class="site-info">
			<?php storycle_footer_copyright(); ?>
		</div><!-- .site-info -->
	</div>
</div><!-- .footer-container -->
