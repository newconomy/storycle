<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

?>
<div <?php storycle_header_container_class(); ?>>
	<div class="container">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php storycle_header_logo() ?>
				<?php storycle_site_description(); ?>
			</div>

			<?php storycle_main_menu(); ?>
		</div>
	</div>
</div><!-- .header-container -->
