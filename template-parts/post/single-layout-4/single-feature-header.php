<?php
/**
 * Template part for displaying featured-header the single posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

$grid_classes = ( 'fullwidth' === storycle_get_mod( 'sidebar_position' ) ) ? array( 'col-xs-12', 'col-xl-8', 'col-xl-push-2' ) : array( 'col-xs-12', 'col-xl-8' );

while ( have_posts() ) : the_post(); ?>

	<div class="single-featured-header">

		<figure class="post-thumbnail"><?php
			storycle_utility()->utility->media->get_image( array(
				'size'        => 'storycle-thumb-xxl',
				'mobile_size' => 'storycle-thumb-xxl',
				'html'        => '<img class="post-thumbnail__img wp-post-image" src="%3$s" alt="%4$s" %5$s>',
				'placeholder' => false,
				'echo'        => true,
			) );
		?></figure><!-- .post-thumbnail -->

		<div class="single-featured-header__inner-wrap invert">
			<div <?php echo storycle_get_container_classes( array( 'single-featured-header__container' ), 'content' ) ?>>
				<div class="row">
					<div class="<?php echo join( ' ', $grid_classes ); ?>">
						<div class="entry-meta entry-meta-top"><?php
							storycle_get_template_part( 'template-parts/post/post-meta/meta-categories' );
							storycle_get_template_part( 'template-parts/post/post-meta/meta-rating' );
							storycle_get_template_part( 'template-parts/post/post-meta/meta-reading-time' );
						?></div><!-- .entry-meta -->

						<header class="entry-header">
							<?php storycle_get_template_part( 'template-parts/post/post-components/post-title' ); ?>
						</header><!-- .entry-header -->

						<div class="entry-excerpt"><?php
							the_excerpt();
						?></div><!-- .entry-excerpt -->

						<div class="entry-meta entry-meta-main"><?php
							storycle_get_template_part( 'template-parts/post/post-meta/meta-author' );
							storycle_get_template_part( 'template-parts/post/post-meta/meta-date' );
							storycle_get_template_part( 'template-parts/post/post-meta/meta-view' );
							storycle_get_template_part( 'template-parts/post/post-meta/meta-comments' );
						?></div><!-- .entry-meta -->
					</div>
				</div>
			</div>
		</div>

	</div><!-- .single-featured-header -->

<?php endwhile;
