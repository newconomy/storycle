<?php
/**
 * The template part for displaying page preloader.
 *
 * @package Storycle
 */
?>
<div class="page-preloader-cover">
	<?php storycle_preloader_logo(); ?>
	<div class="bar"></div>
</div>
