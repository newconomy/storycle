<?php
/**
 * The base template for displaying layout-6 single posts.
 *
 * @package Storycle
 */

get_header( storycle_template_base() ); ?>

<?php storycle_site_breadcrumbs(); ?>

	<div <?php storycle_content_wrap_class(); ?>>

		<?php storycle_get_template_part( 'template-parts/post/single-layout-6/single-feature-header', get_post_format() ) ?>

		<div class="row">

			<div id="primary" <?php storycle_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include storycle_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php. ?>

		</div><!-- .row -->

	</div><!-- .site-content__wrap -->

<?php get_footer( storycle_template_base() );
