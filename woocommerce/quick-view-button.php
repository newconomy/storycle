<?php
/**
 * Quick view button template
 * Note: please, do not remove $this->button_data_attr() function from button when will rewrite template
 *
 */
?>
<div class="tm-quick-view">
	<a href="#" class="tm-quick-view-btn"<?php echo wp_kses_post( $this->button_data_attr() ); ?>>
		<i class="mdi mdi-eye-outline"></i>
	</a>
</div>
