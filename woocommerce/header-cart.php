<?php
/**
 * Display Header Cart
 *
 * @since  1.0.0
 * @return void
 */
?>
<div class="site-header-cart">
	<div class="site-header-cart__wrapper">
		<?php storycle_cart_link(); ?>
		<div class="shopping_cart-dropdown-wrap products_in_cart_<?php echo WC()->cart->get_cart_contents_count(); ?>">
			<div class="shopping_cart-header">
				<h4><?php esc_html_e( 'My Cart', 'storycle' ) ?></h4>
			</div>
			<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
		</div>
	</div>
</div>
