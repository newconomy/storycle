<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Storycle
 */
$sidebar_position = storycle_get_mod( 'sidebar_position' );

if ( 'fullwidth' === $sidebar_position ) {
	return;
}

if ( is_active_sidebar( 'sidebar' ) && ! storycle_is_product_page() ) {
	do_action( 'storycle_render_widget_area', 'sidebar' );
}

if ( is_active_sidebar( 'shop-sidebar' ) && storycle_is_product_page() ) {
	do_action( 'storycle_render_widget_area', 'shop-sidebar' );
}
