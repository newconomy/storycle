<?php
/**
 * Theme Core config file.
 *
 * @var array
 *
 * @package Storycle
 */
$config = array(
	'dashboard_page_name' => esc_html__( 'Storycle Theme', 'storycle' ),
	'library_button'      => false,
	'menu_icon'           => 'dashicons-admin-generic',

	'guide' => array(
		'enabled' => false,
	),

	'api' => array(
		'enabled' => false,
	),
);
