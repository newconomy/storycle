<?php
/**
 * Term meta configuration.
 *
 * @package Storycle
 */

add_action( 'after_setup_theme', 'storycle_init_term_meta', 10 );

/**
 * Add `Inherit` value to options.
 *
 * @param array $options Options.
 *
 * @return array
 */
function storycle_prepare_meta_options( $options = array() ) {
	return array_merge(
		array(
			'inherit' => esc_html__( 'Inherit', 'storycle' ),
		),
		$options
	);
}

/**
 * Init term meta.
 */
function storycle_init_term_meta() {

	$fields = array(
		'storycle_term_settings' => array(
			'element'     => 'section',
			'scroll'      => false,
			'title'       => esc_html__( 'Archive Page Layout Settings', 'storycle' ),
			'description' => esc_html__( 'You can override the global settings for a archive page. If you select inherit option, global setting will be applied.', 'storycle' ),
		),

		'storycle_term_tabs' => array(
			'element' => 'component',
			'type'    => 'component-accordion',
			'parent'  => 'storycle_term_settings',
		),

		'layout_tab' => array(
			'element' => 'settings',
			'parent'  => 'storycle_term_tabs',
			'title'   => esc_html__( 'Layout Settings', 'storycle' ),
		),

		'meta_tab' => array(
			'element' => 'settings',
			'parent'  => 'storycle_term_tabs',
			'title'   => esc_html__( 'Post Meta Settings', 'storycle' ),
		),

		'sidebar_position' => array(
			'type'    => 'select',
			'parent'  => 'layout_tab',
			'title'   => esc_html__( 'Sidebar Position', 'storycle' ),
			'options' => storycle_prepare_meta_options(
				array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'storycle' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'storycle' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'storycle' ),
				)
			),
			'value'   => 'inherit',
		),

		'blog_layout_type' => array(
			'type'    => 'select',
			'parent'  => 'layout_tab',
			'title'   => esc_html__( 'Layout', 'storycle' ),
			'options' => storycle_prepare_meta_options( storycle_get_blog_layouts() ),
			'value'   => 'inherit',
		),

		'blog_layout_columns' => array(
			'type'        => 'select',
			'parent'      => 'layout_tab',
			'title'       => esc_html__( 'Columns', 'storycle' ),
			'description' => esc_html__( 'This setting is used for grid and masonry layouts.', 'storycle' ),
			'options'     => storycle_prepare_meta_options( array(
				'2-cols' => esc_html__( '2 columns', 'storycle' ),
				'3-cols' => esc_html__( '3 columns', 'storycle' ),
				'4-cols' => esc_html__( '4 columns', 'storycle' ),
			) ),
			'value'       => 'inherit',
		),

		'posts_per_page' => array(
			'type'      => 'stepper',
			'parent'    => 'layout_tab',
			'value'     => '',
			'min_value' => 1,
			'max_value' => 100,
			'title'     => esc_html__( 'Posts Number', 'storycle' ),
		),

		'blog_pagination_type' => array(
			'type'    => 'select',
			'parent'  => 'layout_tab',
			'title'   => esc_html__( 'Pagination Type', 'storycle' ),
			'options' => storycle_prepare_meta_options( array(
				'default'   => esc_html__( 'Default', 'storycle' ),
				'load-more' => esc_html__( 'Load More', 'storycle' ),
			) ),
			'value'   => 'inherit',
		),

		'blog_posts_content' => array(
			'type'    => 'select',
			'parent'  => 'layout_tab',
			'title'   => esc_html__( 'Post content', 'storycle' ),
			'options' => storycle_prepare_meta_options( array(
				'excerpt' => esc_html__( 'Only excerpt', 'storycle' ),
				'full'    => esc_html__( 'Full content', 'storycle' ),
				'none'    => esc_html__( 'Hide', 'storycle' ),
			) ),
			'value'   => 'inherit',
		),

		'blog_read_more_btn' => array(
			'type'    => 'radio',
			'parent'  => 'layout_tab',
			'title'   => esc_html__( 'Show Read More button', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),

		'blog_post_author' => array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show post author', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),

		'blog_post_publish_date' => array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show publish date', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),

		'blog_post_categories' => array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show categories', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),

		'blog_post_tags' => array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show tags', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),

		'blog_post_comments' => array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show comments', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		),
	);

	if ( storycle_is_cherry_trending_posts_activated() ) {
		$fields['blog_post_trend_views'] = array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show views counter', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		);

		$fields['blog_post_trend_rating'] = array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show rating', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		);
	}

	if ( storycle_is_cherry_trending_posts_activated() ) {
		$fields['blog_post_share_buttons'] = array(
			'type'    => 'radio',
			'parent'  => 'meta_tab',
			'title'   => esc_html__( 'Show social sharing buttons', 'storycle' ),
			'options' => storycle_enable_disable_radio_options(),
			'value'   => 'inherit',
		);
	}

	if ( class_exists( 'Storycle_Term_Meta' ) ) {
		new Storycle_Term_Meta( array(
			'tax'      => 'category',
			'priority' => 20,
			'fields'   => $fields,
			'meta_key' => 'storycle_term_layout_settings',
		) );

		new Storycle_Term_Meta( array(
			'tax'      => 'post_tag',
			'priority' => 20,
			'fields'   => $fields,
			'meta_key' => 'storycle_term_layout_settings',
		) );
	}
}
