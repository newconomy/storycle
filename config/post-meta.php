<?php
/**
 * Post meta configuration.
 *
 * @package Storycle
 */

add_action( 'after_setup_theme', 'storycle_init_post_options_meta', 10 );
add_action( 'after_setup_theme', 'storycle_init_page_setting_meta', 10 );

/**
 * Enable/disable radio options.
 *
 * @return array
 */
function storycle_enable_disable_radio_options() {
	return array(
		'inherit' => array(
			'label'   => esc_html__( 'Inherit', 'storycle' ),
			'img_src' => get_parent_theme_file_uri( 'assets/images/admin/inherit.svg' ),
		),
		'true' => array(
			'label'   => esc_html__( 'Enable', 'storycle' ),
			'img_src' => get_parent_theme_file_uri( 'assets/images/admin/enable.svg' ),
		),
		'false' => array(
			'label'   => esc_html__( 'Disable', 'storycle' ),
			'img_src' => get_parent_theme_file_uri( 'assets/images/admin/disable.svg' ),
		),
	);
}

/**
 *  Init post options meta.
 */
function storycle_init_post_options_meta() {

	storycle_theme()->get_core()->init_module( 'cherry-post-meta', apply_filters( 'storycle_post_options_meta_args', array(
		'id'            => 'storycle-post-options',
		'title'         => esc_html__( 'Post Options', 'storycle' ),
		'page'          => array( 'post' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'callback_args' => false,
		'fields'        => array(
			'storycle-post-tabs' => array(
				'element' => 'component',
				'type'    => 'component-tab-vertical',
			),

			/** `Post Settings` tab */
			'storycle-settings-tab' => array(
				'element'     => 'settings',
				'parent'      => 'storycle-post-tabs',
				'title'       => esc_html__( 'Post Settings', 'storycle' ),
				'description' => esc_html__( 'You can override the global settings for a single post. If you select inherit option, global setting will be applied.', 'storycle' ),
			),

			'storycle_single_post_author' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show post author', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_publish_date' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show publish date', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_categories' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show categories', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_tags' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show tags', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_comments' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show comments', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_reading_time' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show reading time', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_trend_views' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show views counter', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_trend_rating' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show rating', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_share_buttons' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show social sharing buttons', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_sources' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show source', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_via' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show via', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_respond_button' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show the respond button after post content', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_author_block' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show the author block after each post', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_navigation' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show post navigation', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_single_post_reading_progress' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show reading progress bar', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			'storycle_related_posts_visible' => array(
				'type'    => 'radio',
				'parent'  => 'storycle-settings-tab',
				'title'   => esc_html__( 'Show related posts block', 'storycle' ),
				'value'   => 'inherit',
				'options' => storycle_enable_disable_radio_options(),
			),

			/** `Post Featured` tab */
			'storycle-featured-tab' => array(
				'element' => 'settings',
				'parent'  => 'storycle-post-tabs',
				'title'   => esc_html__( 'Post Format Featured', 'storycle' ),
			),

			'storycle-post-video-source-type' => array(
				'type'        => 'radio',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Video Source Type', 'storycle' ),
				'description' => esc_html__( 'Choose video source type. This setting is used for your video post formats.', 'storycle' ),
				'options'     => array(
					'embed' => array(
						'label' => esc_html__( 'Embed URL', 'storycle' ),
						'slave' => 'embed-video',
					),
					'media-library' => array(
						'label' => esc_html__( 'Media Library', 'storycle' ),
						'slave' => 'media-library-video',
					),
				),
				'value'       => 'embed',
			),

			'storycle-post-video-embed-url' => array(
				'type'        => 'text',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Video Embed URL', 'storycle' ),
				'description' => esc_html__( 'Enter a URL that is compatible with WP built-in oEmbed feature. This setting is used for your video post formats.', 'storycle' ) . ' <a href="http://codex.wordpress.org/Embeds" target="_blank">' .  esc_html__( 'Learn More', 'storycle' ) .'</a>',
				'placeholder' => esc_html__( 'URL', 'storycle' ),
				'master'      => 'embed-video',
			),

			'storycle-post-wp-video' => array(
				'type'               => 'media',
				'parent'             => 'storycle-featured-tab',
				'value'              => '',
				'multi_upload'       => false,
				'library_type'       => 'video',
				'title'              => esc_html__( 'Video', 'storycle' ),
				'description'        => esc_html__( 'Add video from the media library. This setting is used for your video post formats.', 'storycle' ),
				'upload_button_text' => esc_html__( 'Add Video', 'storycle' ),
				'master'             => 'media-library-video',
			),

			'storycle-post-audio-source-type' => array(
				'type'        => 'radio',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Audio Source Type', 'storycle' ),
				'description' => esc_html__( 'Choose audio source type. This setting is used for your audio post formats.', 'storycle' ),
				'options'     => array(
					'embed' => array(
						'label' => esc_html__( 'Embed URL', 'storycle' ),
						'slave' => 'embed-audio',
					),
					'media-library' => array(
						'label' => esc_html__( 'Media Library', 'storycle' ),
						'slave' => 'media-library-audio',
					),
				),
				'value'       => 'embed',
			),

			'storycle-post-audio-embed-url' => array(
				'type'        => 'text',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Audio Embed URL', 'storycle' ),
				'description' => esc_html__( 'Enter a URL that is compatible with WP built-in oEmbed feature. This setting is used for your audio post formats.', 'storycle' ) . ' <a href="http://codex.wordpress.org/Embeds" target="_blank">' .  esc_html__( 'Learn More', 'storycle' ) .'</a>',
				'placeholder' => esc_html__( 'URL', 'storycle' ),
				'master'      => 'embed-audio',
			),

			'storycle-post-wp-audio' => array(
				'type'               => 'media',
				'parent'             => 'storycle-featured-tab',
				'value'              => '',
				'multi_upload'       => false,
				'library_type'       => 'audio',
				'title'              => esc_html__( 'Audio', 'storycle' ),
				'description'        => esc_html__( 'Add audio from the media library. This setting is used for your audio post formats.', 'storycle' ),
				'upload_button_text' => esc_html__( 'Add Audio', 'storycle' ),
				'master'             => 'media-library-audio',
			),

			'storycle-post-gallery' => array(
				'type'               => 'media',
				'parent'             => 'storycle-featured-tab',
				'value'              => '',
				'multi_upload'       => true,
				'library_type'       => 'image',
				'title'              => esc_html__( 'Image Gallery', 'storycle' ),
				'description'        => esc_html__( 'Choose image(s) for the gallery. This setting is used for your gallery post formats.', 'storycle' ),
				'upload_button_text' => esc_html__( 'Add Image(s)', 'storycle' ),
			),

			'storycle-post-link' => array(
				'type'        => 'text',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Link', 'storycle' ),
				'description' => esc_html__( 'Enter your external url. This setting is used for your link post formats.', 'storycle' ),
				'placeholder' => esc_html__( 'URL', 'storycle' ),
			),

			'storycle-post-link-target' => array(
				'type'        => 'select',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Link Target', 'storycle' ),
				'description' => esc_html__( 'Choose your target for the url. This setting is used for your link post formats.', 'storycle' ),
				'options'     => array(
					'_self'  => esc_html__( 'The same frame as it was clicked', 'storycle' ),
					'_blank' => esc_html__( 'A new window or tab', 'storycle' ),
				),
				'value'       => '_self',
			),

			'storycle-post-quote' => array(
				'type'        => 'textarea',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Quote', 'storycle' ),
				'description' => esc_html__( 'Enter your quote. This setting is used for your quote post formats.', 'storycle' ),
				'placeholder' => esc_html__( 'Quote text', 'storycle' ),
			),

			'storycle-post-quote-cite' => array(
				'type'        => 'text',
				'parent'      => 'storycle-featured-tab',
				'title'       => esc_html__( 'Quote Cite', 'storycle' ),
				'description' => esc_html__( 'Enter the quote source. This setting is used for your quote post formats.', 'storycle' ),
				'placeholder' => esc_html__( 'Cite', 'storycle' ),
			),

			/** `Post Source and Via` tab */
			'storycle-sources-via-tab' => array(
				'element' => 'settings',
				'parent'  => 'storycle-post-tabs',
				'title'   => esc_html__( 'Post Source and Via', 'storycle' ),
			),

			'storycle-post-sources' => array(
				'type'        => 'repeater',
				'parent'      => 'storycle-sources-via-tab',
				'title'       => esc_html__( 'Post Source', 'storycle' ),
				'description' => esc_html__( 'Add the links to the sources which provided information for the post.', 'storycle' ),
				'label'       => esc_html__( 'Post Sources', 'storycle' ),
				'add_label'   => esc_html__( 'Add Post Source', 'storycle' ),
				'title_field' => 'label',
				'fields'      => array(
					'label' => array(
						'type'        => 'text',
						'id'          => 'label',
						'name'        => 'label',
						'placeholder' => esc_html__( 'Label', 'storycle' ),
						'label'       => esc_html__( 'Label*', 'storycle' ),
					),
					'url'   => array(
						'type'        => 'text',
						'id'          => 'url',
						'name'        => 'url',
						'placeholder' => esc_html__( 'URL', 'storycle' ),
						'label'       => esc_html__( 'URL*', 'storycle' ),
					),
				),
			),

			'storycle-post-via' => array(
				'type'        => 'repeater',
				'parent'      => 'storycle-sources-via-tab',
				'title'       => esc_html__( 'Via', 'storycle' ),
				'description' => esc_html__( 'Add the link to the source where the post is currently published.', 'storycle' ),
				'label'       => esc_html__( 'Via', 'storycle' ),
				'add_label'   => esc_html__( 'Add Via', 'storycle' ),
				'title_field' => 'label',
				'fields'      => array(
					'label' => array(
						'type'        => 'text',
						'id'          => 'label',
						'name'        => 'label',
						'placeholder' => esc_html__( 'Label', 'storycle' ),
						'label'       => esc_html__( 'Label*', 'storycle' ),
					),
					'url'   => array(
						'type'        => 'text',
						'id'          => 'url',
						'name'        => 'url',
						'placeholder' => esc_html__( 'URL', 'storycle' ),
						'label'       => esc_html__( 'URL*', 'storycle' ),
					),
				),
			),
		),
	) ) );
}

/**
 *  Init page settings meta.
 */
function storycle_init_page_setting_meta() {

	storycle_theme()->get_core()->init_module( 'cherry-post-meta', apply_filters( 'storycle_page_settings_meta_args',  array(
		'id'            => 'page-settings',
		'title'         => esc_html__( 'Page settings', 'storycle' ),
		'page'          => array( 'post', 'page' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'callback_args' => false,
		'fields'        => array(
			'tabs' => array(
				'element' => 'component',
				'type'    => 'component-tab-horizontal',
			),

			/** `Layout Options` tab */
			'layout_tab' => array(
				'element' => 'settings',
				'parent'  => 'tabs',
				'title'   => esc_html__( 'Layout Options', 'storycle' ),
			),
			'storycle_page_layout_style' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Page Layout Style', 'storycle' ),
				'description'   => esc_html__( 'Page layout style global settings redefining. If you select inherit option, global setting will be applied for this layout', 'storycle' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => array(
					'inherit'   => array(
						'label'   => esc_html__( 'Inherit', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/inherit.svg' ),
					),
					'boxed'     => array(
						'label'   => esc_html__( 'Boxed', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/type-boxed.svg' ),
					),
					'framed' => array(
						'label'   => esc_html__( 'Framed', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/type-framed.svg' ),
					),
					'fullwidth' => array(
						'label'   => esc_html__( 'Fullwidth', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/type-fullwidth.svg' ),
					),
				),
			),
			'storycle_sidebar_position' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Sidebar layout', 'storycle' ),
				'description'   => esc_html__( 'Sidebar position global settings redefining. If you select inherit option, global setting will be applied for this layout', 'storycle' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => array(
					'inherit' => array(
						'label'   => esc_html__( 'Inherit', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/inherit.svg' ),
					),
					'one-left-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on left side', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/left-sidebar.svg' ),
					),
					'one-right-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on right side', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/right-sidebar.svg' ),
					),
					'fullwidth' => array(
						'label'   => esc_html__( 'No sidebar', 'storycle' ),
						'img_src' => get_parent_theme_file_uri( 'assets/images/admin/no-sidebar.svg' ),
					),
				),
			),

			/** `Breadcrumbs` tab */
			'breadcrumbs_tab' => array(
				'element' => 'settings',
				'parent'  => 'tabs',
				'title'   => esc_html__( 'Breadcrumbs', 'storycle' ),
			),
			'storycle_breadcrumbs_visibillity' => array(
				'type'          => 'radio',
				'parent'        => 'breadcrumbs_tab',
				'title'         => esc_html__( 'Breadcrumbs visibility', 'storycle' ),
				'description'   => esc_html__( 'Breadcrumbs visibility global settings redefining. If you select inherit option, global setting will be applied for this layout', 'storycle' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => storycle_enable_disable_radio_options(),
			),
		),
	) ) );
}
