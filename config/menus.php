<?php
/**
 * Menus configuration.
 *
 * @package Storycle
 */

add_action( 'after_setup_theme', 'storycle_register_menus', 5 );
/**
 * Register menus.
 */
function storycle_register_menus() {

	register_nav_menus( array(
		'main'   => esc_html__( 'Main', 'storycle' ),
	) );
}
