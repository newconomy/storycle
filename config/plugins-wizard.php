<?php
/**
 * Jet Plugins Wizard configuration.
 *
 * @package Storycle
 */
$license = array(
	'enabled' => false,
);

/**
 * Plugins configuration
 *
 * @var array
 */
$plugins = array(
	'jet-data-importer' => array(
		'name'   => esc_html__( 'Jet Data Importer', 'storycle' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/ZemezLab/jet-data-importer/archive/master.zip',
		'access' => 'base',
	),

	'cherry-ld-mods-switcher' => array(
		'name'   => esc_html__( 'Cherry Live Demo Mods Switcher', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/cherry-ld-mods-switcher.zip' ),
		'access' => 'base',
	),
	'cherry-socialize' => array(
		'name'   => esc_html__( 'Cherry Socialize', 'storycle' ),
		'access' => 'skins',
	),
	'cherry-popups' => array(
		'name'   => esc_html__( 'Cherry Cherry PopUps', 'storycle' ),
		'access' => 'skins',
	),
	'cherry-trending-posts' => array(
		'name'   => esc_html__( 'Cherry Trending Posts', 'storycle' ),
		'access' => 'skins',
	),
	'tm-style-switcher' => array(
		'name'   => esc_html__( 'TM Style Switcher', 'storycle' ),
		'access' => 'skins',
	),

	'elementor' => array(
		'name'   => esc_html__( 'Elementor Page Builder', 'storycle' ),
		'access' => 'base',
	),
	'jet-blog' => array(
		'name'   => esc_html__( 'Jet Blog For Elementor', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-blog.zip' ),
		'access' => 'base',
	),
	'jet-blocks' => array(
		'name'   => esc_html__( 'Jet Blocks For Elementor', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-blocks.zip' ),
		'access' => 'base',
	),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements For Elementor', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-elements.zip' ),
		'access' => 'base',
	),
	'jet-tabs' => array(
		'name'   => esc_html__( 'Jet Tabs For Elementor', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-tabs.zip' ),
		'access' => 'base',
	),
	'jet-theme-core' => array(
		'name'   => esc_html__( 'Jet Theme Core', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-theme-core.zip' ),
		'access' => 'base',
	),
	'jet-tricks' => array(
		'name'   => esc_html__( 'Jet Tricks', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-tricks.zip' ),
		'access' => 'base',
	),
	'jet-menu' => array(
		'name'   => esc_html__( 'Jet Menu', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-menu.zip' ),
		'access' => 'base',
	),
	'jet-reviews' => array(
		'name'   => esc_html__( 'Jet Reviews For Elementor', 'storycle' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( 'assets/includes/plugins/jet-reviews.zip' ),
		'access' => 'base',
	),

	'better-recent-comments' => array(
		'name'   => esc_html__( 'Better Recent Comments', 'storycle' ),
		'access' => 'skins',
	),
	'highlight-and-share' => array(
		'name'   => esc_html__( 'Highlight and Share', 'storycle' ),
		'access' => 'skins',
	),
	'stock-ticker' => array(
		'name'   => esc_html__( 'Stock Ticker', 'storycle' ),
		'access' => 'skins',
	),
	'wordpress-social-login' => array(
		'name'   => esc_html__( 'WordPress Social Login', 'storycle' ),
		'access' => 'skins',
	),

	'amp' => array(
		'name'   => esc_html__( 'AMP', 'storycle' ),
		'access' => 'skins',
	),

	'woocommerce' => array(
		'name'   => esc_html__( 'Woocommerce', 'storycle' ),
		'access' => 'skins',
	),
	'tm-woocommerce-ajax-filters' => array(
		'name'   => esc_html__( 'TM Woocommerce Ajax Filters', 'storycle' ),
		'source' => 'remote',
		'path'   => 'https://github.com/ZemezLab/tm-woocommerce-ajax-filters/archive/master.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-quick-view' => array(
		'name'   => esc_html__( 'TM WooCommerce Quick View', 'storycle' ),
		'source' => 'remote',
		'path'   => 'https://github.com/CherryFramework/tm-woocommerce-quick-view/archive/master.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-compare-wishlist' => array(
		'name'   => esc_html__( 'TM Woocommerce Compare Wishlist', 'storycle' ),
		'access' => 'skins',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'storycle' ),
		'access' => 'skins',
	),
);

/**
 * Skins configuration
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'jet-data-importer',
		'elementor',
		'jet-blog',
		'jet-blocks',
		'jet-elements',
		'jet-tabs',
		'jet-theme-core',
		'jet-tricks',
		'jet-menu',
		'jet-reviews',
		'cherry-ld-mods-switcher',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',

				'better-recent-comments',
				'highlight-and-share',
				'stock-ticker',
				'wordpress-social-login',
				'amp',

				'woocommerce',
				'tm-woocommerce-ajax-filters',
				'tm-woocommerce-compare-wishlist',
				'tm-woocommerce-quick-view',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/storycle/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default/default.jpg',
			'name'  => esc_html__( 'News Portal — 24 Storycle', 'storycle' ),
		),
		'skin-1' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/fashion/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/fashion-daily/fashion-daily.jpg',
			'name'  => esc_html__( 'Fashion Blog — Fashion Daily', 'storycle' ),
		),
		'skin-2' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/lifestyle/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/slife/slife.jpg',
			'name'  => esc_html__( 'Lifestyle — Slife', 'storycle' ),
		),
		'skin-3' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/personal/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/alice-burton/alice-burton.jpg',
			'name'  => esc_html__( 'Personal Blog — Alice Burton', 'storycle' ),
		),
		'skin-4' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/sport/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/championnews/championnews.jpg',
			'name'  => esc_html__( 'Sports — ChampionNews', 'storycle' ),
		),
		'skin-5' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
				'better-recent-comments',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/tech/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/techguide/techguide.jpg',
			'name'  => esc_html__( 'Tech Blog — Techguide', 'storycle' ),
		),
		'skin-6' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
				'stock-ticker',
				'highlight-and-share',
				'better-recent-comments',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/investory/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/investory/investory.jpg',
			'name'  => esc_html__( 'Corporate Blog — Investory', 'storycle' ),
		),
		'skin-7' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
				'highlight-and-share',
				'stock-ticker',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/cryprate/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/cryprate/cryprate.jpg',
			'name'  => esc_html__( 'Crypto-currency Blog — Cryprate', 'storycle' ),
		),
		'skin-8' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
				'highlight-and-share',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/famelle/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/famelle/famelle.jpg',
			'name'  => esc_html__( 'Woman Blog — Feminine', 'storycle' ),
		),
		'skin-9' => array(
			'full'  => array(
				'cherry-trending-posts',
				'tm-style-switcher',
				'highlight-and-share',
				'contact-form-7',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/photto/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/photto/photto.jpg',
			'name'  => esc_html__( 'Photographer Blog — Photto', 'storycle' ),
		),
		'skin-11' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-trending-posts',
				'tm-style-switcher',
				'wordpress-social-login',
				'better-recent-comments',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/voyagin/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/voyagin/voyagin.jpg',
			'name'  => esc_html__( 'Travel Blog — Voyagin', 'storycle' ),
		),
		'skin-12' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-trending-posts',
				'tm-style-switcher',
				'highlight-and-share',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/imagicy/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/imagicy/imagicy.jpg',
			'name'  => esc_html__( 'Architecture — Imagicy', 'storycle' ),
		),
		'skin-13' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-trending-posts',
				'tm-style-switcher',
				'highlight-and-share',
				'wordpress-social-login',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/publicon/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/publicon/publicon.jpg',
			'name'  => esc_html__( 'Newspaper — Publicon', 'storycle' ),
		),
		'skin-14' => array(
			'full'  => array(
				'cherry-popups',
				'cherry-socialize',
				'cherry-trending-posts',
				'tm-style-switcher',

				'better-recent-comments',
				'highlight-and-share',
				'wordpress-social-login',
				'amp',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/gastronomix/',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/gastronomix/gastronomix.jpg',
			'name'  => esc_html__( 'Recipe — Gastronomix', 'storycle' ),
		),
	),
);

$texts = array(
	'theme-name' => esc_html__( 'Storycle', 'storycle' ),
);

$config = array(
	'license' => $license,
	'plugins' => $plugins,
	'skins'   => $skins,
	'texts'   => $texts,
);
