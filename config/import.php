<?php
/**
 * Theme import config file.
 *
 * @var array
 *
 * @package Storycle
 */
$config = array(
	'xml' => false,
	'advanced_import' => array(
		'default' => array(
			'label'    => esc_html__( 'News Portal — 24 Storycle', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/default/default.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/default/default.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/storycle/',
		),
		'skin-1' => array(
			'label'    => esc_html__( 'Fashion Blog — Fashion Daily', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/fashion-daily/fashion-daily.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/fashion-daily/fashion-daily.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/fashion/',
		),
		'skin-2' => array(
			'label'    => esc_html__( 'Lifestyle — Slife', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/slife/slife.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/slife/slife.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/lifestyle/',
		),
		'skin-3' => array(
			'label'    => esc_html__( 'Personal Blog — Alice Burton', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/alice-burton/alice-burton.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/alice-burton/alice-burton.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/personal/',
		),
		'skin-4' => array(
			'label'    => esc_html__( 'Sports — ChampionNews', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/championnews/championnews.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/championnews/championnews.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/sport/',
		),
		'skin-5' => array(
			'label'    => esc_html__( 'Tech Blog — Techguide', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/techguide/techguide.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/techguide/techguide.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/tech/',
		),
		'skin-6' => array(
			'label'    => esc_html__( 'Corporate Blog — Investory', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/investory/investory.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/investory/investory.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/investory/',
		),
		'skin-7' => array(
			'label'    => esc_html__( 'Crypto-currency Blog— Cryprate', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/cryprate/cryprate.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/cryprate/cryprate.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/cryprate/',
		),
		'skin-8' => array(
			'label'    => esc_html__( 'Woman Blog — Feminine', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/famelle/famelle.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/famelle/famelle.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/famelle/',
		),
		'skin-9' => array(
			'label'    => esc_html__( 'Photographer Blog — Photto', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/photto/photto.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/photto/photto.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/photto/',
		),
		'skin-11' => array(
			'label'    => esc_html__( 'Travel Blog — Voyagin', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/voyagin/voyagin.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/voyagin/voyagin.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/voyagin/',
		),
		'skin-12' => array(
			'label'    => esc_html__( 'Architecture — Imagicy', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/imagicy/imagicy.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/imagicy/imagicy.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/imagicy/',
		),
		'skin-13' => array(
			'label'    => esc_html__( 'Newspaper — Publicon', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/publicon/publicon.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/publicon/publicon.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/publicon/',
		),
		'skin-14' => array(
			'label'    => esc_html__( 'Recipe — Gastronomix', 'storycle' ),
			'full'     => get_template_directory() . '/assets/demo-content/gastronomix/gastronomix.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/gastronomix/gastronomix.jpg',
			'demo_url' => 'http://ld-wp.template-help.com/wordpress_blog_multipurpose/gastronomix/',
		),
	),
	'import' => array(
		'chunk_size' => 3,
	),
	'slider' => array(
		'path' => 'https://raw.githubusercontent.com/JetImpex/wizard-slides/master/slides.json',
	),
	'success-links' => array(
		'home' => array(
			'label'  => esc_html__( 'View your site', 'storycle' ),
			'type'   => 'primary',
			'target' => '_self',
			'icon'   => 'dashicons-welcome-view-site',
			'desc'   => esc_html__( 'Take a look at your site', 'storycle' ),
			'url'    => home_url( '/' ),
		),
		'customize' => array(
			'label'  => esc_html__( 'Customize your theme', 'storycle' ),
			'type'   => 'primary',
			'target' => '_self',
			'icon'   => 'dashicons-admin-generic',
			'desc'   => esc_html__( 'Proceed to customizing your theme', 'storycle' ),
			'url'    => admin_url( 'customize.php' ),
		),
	),
	'export' => array(
		'options' => array(
			'woocommerce_default_country',
			'woocommerce_techguide_page_id',
			'woocommerce_default_catalog_orderby',
			'techguide_catalog_image_size',
			'techguide_single_image_size',
			'techguide_thumbnail_image_size',
			'woocommerce_cart_page_id',
			'woocommerce_checkout_page_id',
			'woocommerce_terms_page_id',
			'tm_woowishlist_page',
			'tm_woocompare_page',
			'tm_woocompare_enable',
			'tm_woocompare_show_in_catalog',
			'tm_woocompare_show_in_single',
			'tm_woocompare_compare_text',
			'tm_woocompare_remove_text',
			'tm_woocompare_page_btn_text',
			'tm_woocompare_show_in_catalog',

			'site_icon',
			'elementor_cpt_support',
			'elementor_disable_color_schemes',
			'elementor_disable_typography_schemes',
			'elementor_container_width',
			'elementor_css_print_method',
			'elementor_global_image_lightbox',

			'jet-elements-settings',
			'jet_menu_options',

			'highlight-and-share',
			'stockticker_defaults',
			'wsl_settings_social_icon_set',
		),
		'tables' => array(),
	),
);
