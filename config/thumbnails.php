<?php
/**
 * Thumbnails configuration.
 *
 * @package Storycle
 */

add_action( 'after_setup_theme', 'storycle_register_image_sizes', 5 );
/**
 * Register image sizes.
 */
function storycle_register_image_sizes() {
	set_post_thumbnail_size( 370, 260, true );

	// Registers a new image sizes.
	add_image_size( 'storycle-thumb-s', 275, 195, true );    // default small-img, timeline listing
	add_image_size( 'storycle-thumb-m', 550, 385, true );    // default small-img, timeline listing // mobile
	add_image_size( 'storycle-thumb-l', 770, 540, true );    // default listing
	add_image_size( 'storycle-thumb-l-2', 770, 260, true );  // justify listing
	add_image_size( 'storycle-thumb-xl', 1170, 540, true );  // default listing + full-width
	add_image_size( 'storycle-thumb-xxl', 1920, 550, true ); // single full-width

	add_image_size( 'storycle-thumb-masonry', 370, 9999 );           // masonry listing
	add_image_size( 'storycle-author-avatar', 512, 512, true ); // Widget Author bio

	add_image_size( 'storycle-thumb-110-78', 110, 78, true );   // Jet-Blog
	add_image_size( 'storycle-thumb-216-152', 216, 152, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-240-170', 240, 170, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-310-310', 310, 310, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-340-240', 340, 240, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-340-372', 340, 372, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-710-325', 710, 325, true ); // Jet-Blog

	add_image_size( 'storycle-thumb-wishlist', 230, 230, true );  // Wishlist
	add_image_size( 'storycle-thumb-370-340', 370, 340, true ); // Woo
	add_image_size( 'storycle-thumb-listing-line-product', 360, 360, true ); // Woo
}
