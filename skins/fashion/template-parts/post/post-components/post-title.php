<?php
/**
 * Template part for displaying post title.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

$utility          = storycle_utility()->utility;
$sticky           = storycle_sticky_label( false );
$blog_layout_type = storycle_get_mod( 'blog_layout_type' );
$classes          = array( 'entry-title' );

if ( is_single() ) :

	$title_html = '<h1 %1$s>%4$s</h1>';

elseif ( 'default' === $blog_layout_type ) :

	$title_html = '<h2 %1$s>' . $sticky . '<a href="%2$s" rel="bookmark">%4$s</a></h2>';

else :

	$title_html = '<h2 %1$s>' . $sticky . '<a href="%2$s" rel="bookmark">%4$s</a></h2>';

endif;

$utility->attributes->get_title( array(
	'class' => join( ' ', $classes ),
	'html'  => $title_html,
	'echo'  => true,
) );
