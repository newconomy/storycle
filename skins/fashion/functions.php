<?php
/**
 * Fashion skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Modify views html format.
add_filter( 'cherry_trend_posts_views_format', 'storycle_fashion_modify_cherry_posts_views_format', 10, 3 );

// Change image size at posts-list--default-small-img
add_filter('storycle_post_thumbnail_size', 'fashion_storycle_post_thumbnail_size');

/**
 * Modify views html format.
 *
 * @param string $format  Default html format.
 * @param int    $views   Post views.
 * @param int    $post_id Post id.
 *
 * @return string
 */
function storycle_fashion_modify_cherry_posts_views_format( $format, $views = null, $post_id = null ) {

	$additional_class = '';

	if ( $views > 5000 ) {
		$additional_class = ' very-hot';
	} elseif ( $views > 2000 ) {
		$additional_class = ' hot';
	} elseif ( $views > 500 ) {
		$additional_class = ' warm';
	}

	$suffix_text = _n( 'View', 'Views', $views, 'storycle' );

	$format .= ' <span class="cherry-trend-views__suffix' . $additional_class . '">' . $suffix_text . '</span>';

	return $format;
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function fashion_storycle_post_thumbnail_size( $args ){
	$layout = storycle_get_mod( 'blog_layout_type' );

	if ( 'default-small-img' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-thumb-m';
	}

	return $args;
}