<?php
/**
 * Default skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

//Add aditional thumbnail size
add_action('after_setup_theme','storycle_add_thumbnail_image_size');

// Change image size at listing 2 post list
add_filter('storycle_post_thumbnail_size', 'storycle_voyagin_post_thumbnail_size');

/**
 * Modify jet-menu backgrounds options.
 *
 * @param array $bg_options Backgrounds Options.
 *
 * @return array
 */

//Add aditional thumbnail size
function storycle_add_thumbnail_image_size() {
    add_image_size( 'storycle-smart_post_image_size', 770, 433, true );
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function storycle_voyagin_post_thumbnail_size( $args ){
    $layout = storycle_get_mod( 'blog_layout_type' );

    if ( 'default' === $layout && ! is_single() ) {
        $args['size'] = 'storycle-smart_post_image_size';
    }
    elseif ( is_single() ) {
        $args['size'] = 'storycle-smart_post_image_size';
    }
    elseif (  'grid-2' === $layout && ! is_single() ) {
	    $args['size'] = 'storycle-smart_post_image_size';
    }

    return $args;
}