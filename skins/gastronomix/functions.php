<?php
/**
 * Gastronomix skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Add custom blog layout
add_filter( 'storycle_get_blog_layouts', 'storycle_gastoromix_add_custom_blog_layout');

//Custom blog layout image change
add_filter('storycle_post_thumbnail_size', 'storycle_gastronomix_custom_blog_layout_image_size_change');

// Add custom thumbnail size
add_action('after_setup_theme','storycle_gastronomix_add_thumbnail_image_size');

// Rewrite template jet blog template path
add_filter('jet-blog/template-path', 'styorycle_gastronomix_jet_blog_template_path_rewrite');

// Rewrite tag cloud font-size
add_filter( 'widget_tag_cloud_args', 'storycle_gastronomix_customize_tag_cloud' );

/**
 * Add custom blog layout.
 *
 * @param string $data Blog layout array.
 *
 * @return array
 */
function storycle_gastoromix_add_custom_blog_layout( $args ) {
	$args += [ "big_image" => esc_html__( 'Big Image Layout', 'storycle')];
	return $args;
}

function storycle_gastronomix_custom_blog_layout_image_size_change( $args ) {

	$layout = storycle_get_mod( 'blog_layout_type' );
	$sidebar = storycle_get_mod( 'sidebar_position_post' );

	if ( 'big_image' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-big_image_blog_layout';
	}
	elseif ( is_single() ) {
		if( 'fullwidth' !== $sidebar ) {
			$args['size'] = 'storycle-thumb-800-514';
		}
		elseif ( 'fullwidth' === $sidebar ) {
			$args['size'] = 'storycle-thumb-xxl';
		}
	}
	return $args;
}

/**
 * Add additional thumbnail sizes
 *
 */

function storycle_gastronomix_add_thumbnail_image_size() {
	add_image_size( 'storycle-big_image_blog_layout', 630, 630, true );
	add_image_size( 'storycle-thumb-398-275', 398, 275, true );
	add_image_size( 'storycle-thumb-825-460', 825, 460, true );
	add_image_size( 'storycle-thumb-255-170', 255, 170, true );
	add_image_size( 'storycle-thumb-350-240', 350, 240, true );
	add_image_size( 'storycle-thumb-800-514', 800, 514, true );
}

function styorycle_gastronomix_jet_blog_template_path_rewrite( $path ) {
	$path = 'skins/gastronomix/' . $path;
	return $path;
}

function storycle_gastronomix_customize_tag_cloud( $args ) {
	$args['smallest'] = 14;
	$args['largest']  = 14;
	$args['unit']     = 'px';

	return $args;
}
