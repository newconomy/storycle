<?php
/**
 * Template part for posts pagination.
 *
 * @package Storycle
 */
the_posts_pagination(
	array(
		'prev_text' => '<i class="mdi mdi-skip-previous"></i>',
		'next_text' => '<i class="mdi mdi-skip-next"></i>',
	)
);
