<?php
/**
 * Template part for displaying post comments.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */
$utility = storycle_utility()->utility;

if ( 'post' === get_post_type() ) :

	$comment_visible = ( is_single() ) ? storycle_is_meta_visible( 'single_post_comments', 'single' ) : storycle_is_meta_visible( 'blog_post_comments', 'loop' );

	$post_object   = get_post();
	$comment_count = intval( $post_object->comment_count );

	$comment_prefix  = esc_html__('Comments','storycle');

	$html = ( $comment_count > 0 ) ? '<span class="post__comments">%1$s<a href="%2$s" %3$s %4$s>%5$s%6$s ' . $comment_prefix . ' </a></span>' : '<span class="post__comments">%1$s%5$s%6$s ' . $comment_prefix . '</span>';

	$utility->meta_data->get_comment_count( array(
		'visible' => $comment_visible,
		'icon'    => false,
		'html'    => $html,
		'class'   => 'post__comments-link',
		'echo'    => true,
	) );

endif;
