<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card invert' ); ?>>
	<?php storycle_get_template_part( 'template-parts/post/post-meta/meta-timeline-date' ); ?>

	<div class="posts-list__item-inner">
		<div class="posts-list__item-media">
			<div class="post-featured-content"><?php
				storycle_get_post_format_audio( array(
					'width'  => 700,
					'height' => 240,
					'echo'   => true,
				) );
			?></div><!-- .post-featured-content -->
			<?php storycle_get_template_part( 'template-parts/post/post-meta/meta-categories' ); ?>
		</div><!-- .posts-list__item-media -->

		<div class="posts-list__item-content">

			<div class="entry-meta entry-meta-top"><?php
				storycle_get_template_part( 'template-parts/post/post-meta/meta-rating' );
				storycle_get_template_part( 'template-parts/post/post-meta/meta-view' );
			?></div><!-- .entry-meta -->

			<div class="entry-meta entry-meta-main"><?php
				storycle_get_template_part( 'template-parts/post/post-meta/meta-author' );
				storycle_get_template_part( 'template-parts/post/post-meta/meta-date' );
				storycle_get_template_part( 'template-parts/post/post-meta/meta-comments' );
			?></div><!-- .entry-meta -->

            <header class="entry-header"><?php
				storycle_get_template_part( 'template-parts/post/post-components/post-title' );
				?></header><!-- .entry-header -->

			<div class="entry-content"><?php
				storycle_get_template_part( 'template-parts/post/post-components/post-content' );
			?></div><!-- .entry-content -->

			<div class="entry-meta entry-meta-bottom"><?php
				storycle_get_template_part( 'template-parts/post/post-meta/meta-tags' );
			?></div><!-- .entry-meta -->

			<footer class="entry-footer">
				<div class="entry-footer-container"><?php
					storycle_get_template_part( 'template-parts/post/post-components/post-button' );
					storycle_get_template_part( 'template-parts/post/post-components/post-share-buttons' );
				?></div>
			</footer><!-- .entry-footer -->
		</div><!-- .posts-list__item-content -->
	</div><!-- .posts-list__item-inner -->
</article><!-- #post-## -->
