<?php
/**
 * Template part for displaying single posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php storycle_ads_post_before_content() ?>

	<div class="entry-meta entry-meta-main-top"><?php
	  storycle_get_template_part( 'template-parts/post/post-meta/meta-categories' );
	  ?></div><!-- .entry-meta -->

	<header class="entry-header">
	  <?php storycle_get_template_part( 'template-parts/post/post-components/post-title' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-meta entry-meta-main-middle"><?php
	  storycle_get_template_part( 'template-parts/post/post-meta/meta-author' );
	  storycle_get_template_part( 'template-parts/post/post-meta/meta-date' );
	  ?></div><!-- .entry-meta -->

	<div class="entry-meta entry-meta-main-bottom">
		<div class="inner_bootom_meta"><?php
		storycle_get_template_part( 'template-parts/post/post-meta/meta-rating' );
		storycle_get_template_part( 'template-parts/post/post-meta/meta-comments' );
		storycle_get_template_part( 'template-parts/post/post-meta/meta-view' );
		?></div>
		<div class="inner_bootom_meta"><?php
		storycle_get_template_part( 'template-parts/post/post-meta/meta-reading-time' );
		?></div>
	</div><!-- .entry-meta -->

	<div class="post-featured-content"><?php
		storycle_get_template_part( 'template-parts/post/post-components/post-video' );
	?></div><!-- .post-featured-content -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links__title">' . esc_html__( 'Pages:', 'storycle' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span class="page-links__item">',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'storycle' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
		<?php storycle_get_template_part( 'template-parts/post/post-components/post-respond-button' ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer"><?php
		storycle_get_template_part( 'template-parts/post/post-meta/meta-tags' );
		storycle_get_template_part( 'template-parts/post/post-meta/meta-via' );
		storycle_get_template_part( 'template-parts/post/post-meta/meta-sources' );
		storycle_get_template_part( 'template-parts/post/post-components/post-share-buttons' );
	?></footer><!-- .entry-footer -->

</article><!-- #post-## -->
