<?php
/**
 * Template part for displaying post via.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

if ( 'post' === get_post_type() ) :

	storycle_get_post_via( array(
		'visible' => storycle_get_mod( 'single_post_via' ),
		'prefix'  => '<span class="meta-title">' . esc_html__( 'Via', 'storycle' ) . '</span> ',
		'echo'    => true,
	) );

endif;
