<?php
/**
 * Default skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Modify jet-menu backgrounds options.
add_filter( 'jet-menu/menu-css/backgrounds', 'storycle_modify_jet_menu_bg_options' );

// Change image size at listing 2 post list
add_filter('storycle_post_thumbnail_size', 'storycle_famelle_post_thumbnail_size');

/**
 * Modify jet-menu backgrounds options.
 *
 * @param array $bg_options Backgrounds Options.
 *
 * @return array
 */
function storycle_modify_jet_menu_bg_options( $bg_options = array() ){

	$bg_options['jet-menu-item-hover']  = '.jet-menu-item:hover > .top-level-link, .jet-menu .jet-menu-item:hover > .top-level-link:before';
	$bg_options['jet-menu-item-active'] = '.jet-menu-item.jet-current-menu-item .top-level-link, .jet-menu .jet-menu-item.jet-current-menu-item .top-level-link:before';

	return  $bg_options;
}

add_action( 'after_setup_theme', 'storycle_famelle_register_image_sizes', 5 );

function storycle_famelle_register_image_sizes() {
	remove_image_size( 'storycle-thumb-xl', 1170, 540, true );  // default listing + full-width


	add_image_size( 'storycle-thumb-240-144', 240, 144, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-370-221', 370, 221, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-570-340', 570, 340, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-270-161', 270, 161, true ); // Jet-Blog
	add_image_size( 'storycle-thumb-570-400', 570, 400, false ); // Masonry
	add_image_size( 'storycle-thumb-xl', 1170, 700, true );  // default listing + full-width
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function storycle_famelle_post_thumbnail_size( $args ){
	$layout = storycle_get_mod( 'blog_layout_type' );
	$sidebar = storycle_get_mod( 'sidebar_position_post' );

	if ( 'masonry' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-thumb-570-400';
	}
	if ( 'grid' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-thumb-570-340';
	}
	if ( 'grid-2' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-thumb-xl';
	}
	elseif ( is_single() ) {
		if( 'fullwidth' !== $sidebar ) {
			$args['size'] = 'storycle-thumb-xl';
		}
		else {
			$args['size'] ='storycle-thumb-xl';
		}
	}
	return $args;
}
