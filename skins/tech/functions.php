<?php
/**
 * Tech skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Customization for `Tag Cloud` widget.
add_filter( 'widget_tag_cloud_args', 'storycle_tech_skin_customize_tag_cloud' );

/**
 * Customization for `Tag Cloud` widget.
 *
 * @since  1.0.0
 *
 * @param  array $args Widget arguments.
 *
 * @return array
 */
function storycle_tech_skin_customize_tag_cloud( $args ) {
	$args['smallest'] = 20;
	$args['largest']  = 20;
	$args['unit']     = 'px';

	return $args;
}