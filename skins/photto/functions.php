<?php
/**
 * Default skin functions, hooks and definitions.
 *
 * @package Storycle
 */

//Add aditional thumbnail size
add_action('after_setup_theme','storycle_add_thumbnail_image_size');

// Change image size at listing 2 post list
add_filter('storycle_post_thumbnail_size', 'storycle_photto_post_thumbnail_size');

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Modify jet-menu backgrounds options.
add_filter( 'jet-menu/menu-css/backgrounds', 'storycle_modify_jet_menu_bg_options' );

/**
 * Modify jet-menu backgrounds options.
 *
 * @param array $bg_options Backgrounds Options.
 *
 * @return array
 */
function storycle_modify_jet_menu_bg_options( $bg_options = array() ){

	$bg_options['jet-menu-item-hover']  = '.jet-menu-item:hover > .top-level-link, .jet-menu .jet-menu-item:hover > .top-level-link:before';
	$bg_options['jet-menu-item-active'] = '.jet-menu-item.jet-current-menu-item .top-level-link, .jet-menu .jet-menu-item.jet-current-menu-item .top-level-link:before';

	return  $bg_options;
}

function storycle_add_thumbnail_image_size() {
	add_image_size( 'storycle-smart_post_single_image_size', 770, 510, true );
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function storycle_photto_post_thumbnail_size( $args ){
	$layout = storycle_get_mod( 'blog_layout_type' );

	if ( is_single() ) {
		$args['size'] = 'storycle-smart_post_single_image_size';
	}
	elseif (  'default' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-smart_post_single_image_size';
	}

	return $args;
}