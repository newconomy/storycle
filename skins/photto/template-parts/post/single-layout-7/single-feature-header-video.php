<?php
/**
 * Template part for displaying featured-header the single posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */
?>
<div class="single-featured-header card-wrapper">

	<div class="entry-meta entry-meta-top"><?php
	storycle_get_template_part( 'template-parts/post/post-meta/meta-categories' );
	storycle_get_template_part( 'template-parts/post/post-meta/meta-rating' );
	storycle_get_template_part( 'template-parts/post/post-meta/meta-reading-time' );
	?></div><!-- .entry-meta -->

	<header class="entry-header">
		<?php storycle_get_template_part( 'template-parts/post/post-components/post-title' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-meta entry-meta-main"><?php
	storycle_get_template_part( 'template-parts/post/post-meta/meta-date' );
	storycle_get_template_part( 'template-parts/post/post-meta/meta-comments' );
	storycle_get_template_part( 'template-parts/post/post-meta/meta-view' );
	?></div><!-- .entry-meta -->

	<div class="post-featured-content"><?php
	storycle_get_template_part( 'template-parts/post/post-components/post-video' );
	?></div><!-- .post-featured-content -->

</div><!-- .single-featured-header -->
