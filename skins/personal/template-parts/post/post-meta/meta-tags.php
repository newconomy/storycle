<?php
/**
 * Template part for displaying post tags.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

$utility = storycle_utility()->utility;

if ( 'post' === get_post_type() ) :

	$tags_visible = ( is_single() ) ? storycle_is_meta_visible( 'single_post_tags', 'single' ) : storycle_is_meta_visible( 'blog_post_tags', 'loop' );

	$utility->meta_data->get_terms( array(
		'visible'   => $tags_visible,
		'type'      => 'post_tag',
		'icon'      => '',
		'prefix'    => ( ! is_single() ) ? '' : '<span class="meta-title">' . esc_html__( 'Tags', 'storycle' ) . '</span> ',
		'delimiter' => ' ',
		'before'    => '<div class="post__tags">',
		'after'     => '</div>',
		'echo'      => true,
	) );

endif;
