<?php
/**
 * Personal skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Modify views html format.
add_filter( 'cherry_trend_posts_views_format', 'storycle_personal_modify_cherry_posts_views_format', 10, 3 );

// Change image size at timeline post list
add_filter('storycle_post_thumbnail_size', 'personal_storycle_post_thumbnail_size');

// Customization for `Tag Cloud` widget.
add_filter( 'widget_tag_cloud_args', 'storycle_personal_skin_customize_tag_cloud' );

// Modify template path for jet-elements templates.
add_filter( 'jet-elements/template-path', 'storycle_personal_skin_jet_elements_template_path' );

/**
 * Modify views html format.
 *
 * @param string $format  Default html format.
 * @param int    $views   Post views.
 * @param int    $post_id Post id.
 *
 * @return string
 */
function storycle_personal_modify_cherry_posts_views_format( $format, $views = null, $post_id = null ) {

	$additional_class = '';

	if ( $views > 5000 ) {
		$additional_class = ' very-hot';
	} elseif ( $views > 2000 ) {
		$additional_class = ' hot';
	} elseif ( $views > 500 ) {
		$additional_class = ' warm';
	}

	$suffix_text = _n( 'View', 'Views', $views, 'storycle' );

	$format .= ' <span class="cherry-trend-views__suffix' . $additional_class . '">' . $suffix_text . '</span>';

	return $format;
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function personal_storycle_post_thumbnail_size( $args ){
	$layout = storycle_get_mod( 'blog_layout_type' );

	if ( 'timeline' === $layout ) {
		$args['size'] = 'storycle-thumb-m';
	}

	return $args;
}


/**
 * Customization for `Tag Cloud` widget.
 *
 * @since  1.0.0
 *
 * @param  array $args Widget arguments.
 *
 * @return array
 */
function storycle_personal_skin_customize_tag_cloud( $args ) {
	$args['smallest'] = 10;
	$args['largest']  = 10;
	$args['unit']     = 'px';

	return $args;
}

/**
 * Modify template path for jet-elements templates.
 *
 * @param string $path Default path.
 *
 * @return string
 */
function storycle_personal_skin_jet_elements_template_path( $path ) {

	$path = 'skins/personal/' . $path;

	return $path;
}
