<?php
/**
 * Personal skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );

// Add custom thumbnail size
add_action('after_setup_theme','storycle_add_thumbnail_image_size');

// Change image size at listing 2 post list
add_filter('storycle_post_thumbnail_size', 'storycle_investory_post_thumbnail_size');


/**
 * Register custom image sizes.
 */
function storycle_add_thumbnail_image_size() {
	add_image_size( 'storycle-smart_post_list_size', 430, 322, true );
	add_image_size( 'storycle-smart_post_list_small_size', 205, 153, true );
	add_image_size( 'storycle-smart_post_list_thumb-280-210', 280, 210, true );
	add_image_size( 'storycle-post_thumb-220-165', 220, 165, true );
	add_image_size( 'storycle-post_thumb-380-285', 380, 285, true );
	add_image_size( 'storycle-post_thumb-780-450', 780, 450, true );
}

/**
 * Change image size at posts-list--default-small-img
 *
 * @return array
 */

function storycle_investory_post_thumbnail_size( $args ){
	$layout = storycle_get_mod( 'blog_layout_type' );
	$sidebar = storycle_get_mod( 'sidebar_position_post' );

	if ( 'default-small-img' === $layout && ! is_single() ) {
		$args['size'] = 'storycle-post_thumb-380-285';
	}
	elseif ( is_single() ) {
		if( 'fullwidth' !== $sidebar ) {
			$args['size'] = 'storycle-post_thumb-780-450';
		}
		else {
			$args['size'] ='storycle-thumb-xxl';
		}
	}
	return $args;
}