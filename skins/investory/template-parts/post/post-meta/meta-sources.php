<?php
/**
 * Template part for displaying post sources.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

if ( 'post' === get_post_type() ) :

	storycle_get_post_sources( array(
		'visible' => storycle_get_mod( 'single_post_sources' ),
		'prefix'  => '<span class="meta-title">' . esc_html__( 'Source', 'storycle' ) . '</span> ',
		'echo'    => true,
	) );

endif;
