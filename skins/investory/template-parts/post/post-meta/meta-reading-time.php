<?php
/**
 * Template part for displaying post reading time.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

if ( 'post' === get_post_type() ) :

	storycle_get_post_reading_time(
		array(
			'visible' => storycle_get_mod( 'single_post_reading_time' ),
			'echo'    => true,
		)
	);

endif;
