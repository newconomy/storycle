<?php
/**
 * Template part for displaying share buttons.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Storycle
 */

if ( 'post' === get_post_type() ) :

	$share_visible = ( is_single() ) ? storycle_is_meta_visible( 'single_post_share_buttons', 'single' ) : storycle_is_meta_visible( 'blog_post_share_buttons', 'loop' );
	$custom_class  = ( is_single() ) ? 'cs-share--single cs-share--rounded' : 'cs-share--loop cs-share--animate';

	if ( $share_visible ) :

		do_action( 'cherry_socialize_display_sharing',
			true,
			array(
				'custom_class' => $custom_class,
				'before-html'  => '<span class="cs-share__icon"><i class="mdi mdi-share-variant"></i></span><h4 class="cs-share__title">' . esc_html__( 'Share', 'storycle' ) . '</h4>',
			),
			array()
		);

	endif;

endif;
