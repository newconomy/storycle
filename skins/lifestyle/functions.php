<?php
/**
 * Lifestyle skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Customization for `Tag Cloud` widget.
add_filter( 'widget_tag_cloud_args', 'storycle_lifestyle_skin_customize_tag_cloud' );

/**
 * Customization for `Tag Cloud` widget.
 *
 * @since  1.0.0
 *
 * @param  array $args Widget arguments.
 *
 * @return array
 */
function storycle_lifestyle_skin_customize_tag_cloud( $args ) {
	$args['smallest'] = 14;
	$args['largest']  = 14;
	$args['unit']     = 'px';

	return $args;
}