<?php
/**
 * Sport skin functions, hooks and definitions.
 *
 * @package Storycle
 */


// Disable support content separate style.
add_filter( 'storycle_is_support_content_separate_style', '__return_false' );