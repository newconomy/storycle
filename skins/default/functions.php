<?php
/**
 * Default skin functions, hooks and definitions.
 *
 * @package Storycle
 */

// Modify jet-menu backgrounds options.
add_filter( 'jet-menu/menu-css/backgrounds', 'storycle_modify_jet_menu_bg_options' );

/**
 * Modify jet-menu backgrounds options.
 *
 * @param array $bg_options Backgrounds Options.
 *
 * @return array
 */
function storycle_modify_jet_menu_bg_options( $bg_options = array() ){

	$bg_options['jet-menu-item-hover']  = '.jet-menu-item:hover > .top-level-link, .jet-menu .jet-menu-item:hover > .top-level-link:before';
	$bg_options['jet-menu-item-active'] = '.jet-menu-item.jet-current-menu-item .top-level-link, .jet-menu .jet-menu-item.jet-current-menu-item .top-level-link:before';

	return  $bg_options;
}
