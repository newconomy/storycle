<?php
/**
 * Register presets for TM Style Switcher
 *
 * @package Storycle
 */
if ( function_exists( 'tmss_register_preset' ) ) {

	tmss_register_preset(
		'default',
		esc_html__( 'Storycle', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/default/default.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/default.json' )
	);

	tmss_register_preset(
		'fashion-daily',
		esc_html__( 'Fashion Daily', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/fashion-daily/fashion-daily.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/fashion-daily.json' )
	);

	tmss_register_preset(
		'slife',
		esc_html__( 'Slife', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/slife/slife.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/slife.json' )
	);

	tmss_register_preset(
		'personal',
		esc_html__( 'Alice Burton', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/alice-burton/alice-burton.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/personal.json' )
	);

	tmss_register_preset(
		'photto',
		esc_html__( 'Photto', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/photto/photto.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/photto.json' )
	);

	tmss_register_preset(
		'championnews',
		esc_html__( 'ChampionNews', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/championnews/championnews.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/championnews.json' )
	);

	tmss_register_preset(
		'techguide',
		esc_html__( 'TechGuide', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/techguide/techguide.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/techguide.json' )
	);

	tmss_register_preset(
		'investory',
		esc_html__( 'Investory', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/investory/investory.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/investory.json' )
	);

	tmss_register_preset(
		'famelle',
		esc_html__( 'Famelle', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/famelle/famelle.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/famelle.json' )
	);

	tmss_register_preset(
		'cryprate',
		esc_html__( 'Cryprate', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/cryprate/cryprate.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/cryprate.json' )
	);

	tmss_register_preset(
		'voyagin',
		esc_html__( 'Voyagin', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/voyagin/voyagin.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/voyagin.json' )
	);

	tmss_register_preset(
		'imagicy',
		esc_html__( 'Imagicy', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/imagicy/imagicy.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/imagicy.json' )
	);

	tmss_register_preset(
		'publicon',
		esc_html__( 'Publicon', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/publicon/publicon.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/publicon.json' )
	);
	tmss_register_preset(
		'gastronomix',
		esc_html__( 'Gastronomix', 'storycle' ),
		get_parent_theme_file_uri( 'assets/demo-content/gastronomix/gastronomix.jpg' ),
		get_parent_theme_file_path( '/tm-style-switcher-presets/gastronomix.json' )
	);
}
