<?php
/**
 * Storycle WooCommerce Theme hooks.
 *
 * @package Storycle
 */

/**
 * Enable Woocommerce theme support
 */
function storycle_woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

/**
 * Change WooCommerce loop category title layout
 *
 * @param object $category
 *
 * @return string
 */
function storycle_woocommerce_template_loop_category_title( $category ) {
	?>
	<div class="title_count_block">
		<?php if ( 0 < $category->count ) {
			echo '<a href="' . esc_url( get_term_link( $category, 'product_cat' ) ) . '"  class="count">';
			echo apply_filters( 'woocommerce_subcategory_count_html', ' <span><span class="count__number">' . $category->count . '</span> ' . esc_html__( 'products', 'storycle' ) . '</span>', $category );
			echo '</a>';
		}
		?>
		<h3><?php
			echo '<a href="' . esc_url( get_term_link( $category, 'product_cat' ) ) . '">';
			echo wp_kses_post( $category->name );
			echo '</a>';
			?>
		</h3>
	</div>
	<?php
}

/**
 * Arrow position in tm_categories_carousel_widget.
 *
 * @return string
 */
function storycle_tm_categories_carousel_widget_arrows_pos() {
	return 'outside';
}

/**
 * Print sale and date format
 *
 * @return string
 */
function storycle_products_sale_end_date() {
	global $product;
	$sale_end_date = get_post_meta( get_the_id(), '_sale_price_dates_to', true );
	if ( '' != $sale_end_date ) {
		$data_format   = apply_filters( 'tm_products_sale_end_date_format', esc_html__( '%D %H:%M:%S', 'storycle' ) );
		$sale_end_date = '<span class="tm-products-sale-end-date" data-countdown="' . date( 'Y/m/d', $sale_end_date ) . '" data-format="' . htmlentities ( $data_format ) . '"></span>';
	}

	$allowed_html = array(
		'span' => array(
			'data-countdown' => true,
			'data-format'    => true,
		),
	);

	echo wp_kses( $sale_end_date, storycle_kses_post_allowed_html( $allowed_html ) );
}

/**
 * Products sale end date format.
 *
 * @return string
 */
function storycle_products_format_sale_end_date() {
	return sprintf( '<span>%%D <i>%1$s</i></span> <span>%%H <i>%2$s</i></span><span>%%M <i>%3$s</i></span>', esc_html__( 'days', 'storycle' ), esc_html__( 'Hrs', 'storycle' ), esc_html__( 'Min', 'storycle' ) );
}

/**
 * Add WooCommerce 'New' and 'Featured' Flashes
 *
 */
function storycle_woocommerce_show_flash() {
	global $product;

	if ( ! $product->is_on_sale() ) {

		if ( 604800 > ( date( 'U' ) - strtotime( get_the_date() ) ) ) {
			echo '<span class="new">' . esc_html__( 'New', 'storycle' ) . '</span>';
		} else if ( $product->is_featured() ) {
			echo '<span class="featured">' . esc_html__( 'Featured', 'storycle' ) . '</span>';
		}
	}
}

/**
 * Change WooCommerce Price Output when Product is on Sale
 *
 * @param  string     $price Price
 * @param  int|string $from Regular price
 * @param  int|string $to Sale price
 *
 * @return string
 */
function storycle_woocommerce_get_price_html_from_to( $price, $from, $to ) {
	$price = '<ins>' . ( ( is_numeric( $to ) ) ? wc_price( $to ) : $to ) . '</ins> <del>' . ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) . '</del>';

	return $price;
}


/**
 * Open wrap comapre & wishlist button grid-listing
 *
 */
function storycle_compare_wishlist_wrap_open( $atts = '' ) {
	if ( 'list' !== $atts ) {
		echo '<div class="wishlist_compare_button_block">';
	}
}

/**
 * Close wrap comapre & wishlist button grid-listing
 *
 */
function storycle_compare_wishlist_wrap_close( $atts = '' ) {
	if ( 'list' !== $atts ) {
		echo '</div>';
	}
}

/**
 * Open wrap comapre & wishlist button list-listing
 *
 */
function storycle_compare_wishlist_wrap_list_open( $atts = '' ) {
	if ( 'list' === $atts ) {
		echo '<div class="wishlist_compare_button_block">';
	}
}

/**
 * Close wrap comapre & wishlist button list-listing
 *
 */
function storycle_compare_wishlist_wrap_list_close( $atts = '' ) {
	if ( 'list' === $atts ) {
		echo '</div>';
	}
}

/**
 * Rewrite functions compare & wishlist for grid or listing layouts
 *
 */
function storycle_woocompare_add_button_loop( $atts = '' ) {
	if ( 'list' !== $atts && function_exists( 'tm_woocompare_add_button_loop' ) ) {
		tm_woocompare_add_button_loop( $atts );
	}
}

function storycle_woocompare_add_button_loop_list( $atts = '' ) {
	if ( 'list' === $atts && function_exists( 'tm_woocompare_add_button_loop' ) ) {
		tm_woocompare_add_button_loop( $atts );
	}
}

function storycle_woowishlist_add_button_loop( $atts = '' ) {
	if ( 'list' !== $atts && function_exists( 'tm_woowishlist_add_button_loop' ) ) {
		tm_woowishlist_add_button_loop( $atts );
	}
}

function storycle_woowishlist_add_button_loop_list( $atts = '' ) {
	if ( 'list' === $atts && function_exists( 'tm_woowishlist_add_button_loop' ) ) {
		tm_woowishlist_add_button_loop( $atts );
	}
}

/**
 * Rewrite functions rating for grid or listing layouts
 *
 */
function storycle_woocommerce_template_loop_rating( $atts = '' ) {
	if ( 'list' !== $atts ) {
		woocommerce_template_loop_rating( $atts );
	}
}

function storycle_woocommerce_template_loop_rating_list( $atts = '' ) {
	if ( 'list' === $atts ) {
		woocommerce_template_loop_rating( $atts );
	}
}

/**
 * Added custom thumbnail size for listing-line category thumbnail
 *
 */
function storycle_woocommerce_subcategory_thumbnail( $category, $atts = '' ) {
	global $_wp_additional_image_sizes;
	if ( 'list' === $atts ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_list_size', 'storycle-thumb-listing-line-product' );
		$image_sizes          = get_intermediate_image_sizes();

		if ( in_array( $small_thumbnail_size, $image_sizes ) ) {
			$dimensions[ 'width' ]  = $_wp_additional_image_sizes[ $small_thumbnail_size ][ 'width' ];
			$dimensions[ 'height' ] = $_wp_additional_image_sizes[ $small_thumbnail_size ][ 'height' ];
			$dimensions[ 'crop' ]   = $_wp_additional_image_sizes[ $small_thumbnail_size ][ 'crop' ];
		} else {
			$dimensions[ 'width' ]  = '300';
			$dimensions[ 'height' ] = '300';
			$dimensions[ 'crop' ]   = 1;
		}

	} else {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'shop_catalog' );
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
	}

	$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

	if ( $thumbnail_id ) {
		$image = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
		$image = $image[ 0 ];
	} else {
		$image = wc_placeholder_img_src();
	}

	if ( $image ) {
		// Prevent esc_url from breaking spaces in urls for image embeds
		// Ref: https://core.trac.wordpress.org/ticket/23605
		$image = str_replace( ' ', '%20', $image );

		echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions[ 'width' ] ) . '" height="' . esc_attr( $dimensions[ 'height' ] ) . '" />';
	}
}

/**
 * Add links into product title
 *
 */
function storycle_template_loop_product_title() {
	echo '<h3><a href="' . esc_url( get_the_permalink() ) . '" class="woocommerce-LoopProduct-link">' . get_the_title() . '</a></h3>';
}

/**
 * Added custom thumbnail size for listing-line products
 *
 */
function storycle_get_product_thumbnail_size( $atts = '' ) {
	if ( 'list' === $atts ) {
		echo woocommerce_get_product_thumbnail( 'storycle-thumb-listing-line-product' );
	} else {
		echo woocommerce_get_product_thumbnail();
	}
}

/**
 * Wrap content single product
 *
 */
function storycle_single_product_open_wrapper() {
	echo '<div class="single_product_wrapper">';
}

function storycle_single_product_close_wrapper() {
	echo '</div>';
}

/**
 * Open wrap product loop elements
 *
 */
function storycle_product_image_wrap_open() {
	echo '<div class="block_product_thumbnail">';
}

/**
 * Close wrap product loop elements
 *
 */
function storycle_product_image_wrap_close() {
	echo '</div>';
}

/**
 * Open wrap content product loop elements
 *
 */
function storycle_product_content_wrap_open() {
	echo '<div class="block_product_content">';
}

/**
 * Close wrap content product loop elements
 *
 */
function storycle_product_content_wrap_close() {
	echo '</div>';
}

/**
 * Assets woocommerce script
 *
 */
function storycle_enqueue_assets() {

	if ( storycle_is_woocommerce_activated() ) {
		wp_register_script( 'jquery-easyzoom', get_parent_theme_file_uri( 'assets/js/min/easyzoom.min.js' ), array( 'jquery' ), '2.3.1', true );
		wp_enqueue_script( 'storycle-single-product-init', get_parent_theme_file_uri( 'assets/js/single-product.js' ), array( 'jquery', 'jquery-easyzoom' ), '1.0.0', true );
		wp_enqueue_script( 'countdown-script', get_parent_theme_file_uri( 'assets/js/min/jquery.countdown.min.js' ), array( 'jquery' ), '2.1.0', true );
		wp_enqueue_script( 'storycle-woo-theme-script', get_parent_theme_file_uri( 'assets/js/woo-theme-script.js' ), array( 'jquery' ), '1.0.0', true );
	}
}

/**
 * Wrap products carousel - div
 */
function storycle_tm_products_carousel_widget_wrapper_open() {
	return '<div class="swiper-wrapper tm-products-carousel-widget-wrapper products">';
}

function storycle_tm_products_carousel_widget_wrapper_close() {
	return '</div>';
}

/**
 * Change quickview button position
 *
 * @return number
 */
function storycle_tm_woo_quick_view_button_hook( $data ) {
	$atts = '';

	if ( class_exists( 'TM_WC_Ajax_Filters' ) && function_exists( 'tm_wc_grid_list' ) && isset( tm_wc_grid_list()->condition ) ) {
		$atts = tm_wc_grid_list()->condition;
	}

	if ( 'list' === $atts && ( is_archive() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) ) {
		$data[ 'hook' ]     = 'woocommerce_after_shop_loop_item';
		$data[ 'priority' ] = 10;
	} else {
		$data[ 'hook' ]     = 'woocommerce_before_shop_loop_item';
		$data[ 'priority' ] = 3;
	}

	return $data;
}

/**
 * Output the related products.
 *
 * @subpackage  Product
 */
function storycle_woocommerce_output_related_products() {

	$args = array(
		'posts_per_page'    => 10,
		'columns'           => 3,
		'orderby'           => 'rand'
	);

	woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
}

/**
 * Open wrapper for carousel loop products
 *
 * @param bool $echo
 * @param bool $swiper
 *
 * @return string
 */
function storycle_woocommerce_product_loop_carousel_start( $echo = true, $swiper = false ) {

	$uniqid = uniqid();
	$GLOBALS['uniqid'] = $uniqid;
	$slides_per_view = '4';
	$data_attr_line = 'class="storycle-carousel swiper-container"';
	$data_attr_line .= ' data-uniq-id="swiper-carousel-' . $uniqid . '"';
	$data_attr_line .= ' data-slides-per-view="' . $slides_per_view . '"';
	$data_attr_line .= ' data-slides-per-group="1"';
	$data_attr_line .= ' data-slides-per-column="1"';
	$data_attr_line .= ' data-space-between-slides="30"';
	$data_attr_line .= ' data-duration-speed="500"';
	$data_attr_line .= ' data-swiper-loop="false"';
	$data_attr_line .= ' data-free-mode="false"';
	$data_attr_line .= ' data-grab-cursor="true"';
	$data_attr_line .= ' data-mouse-wheel="false"';

	$html = '<div class="swiper-carousel-container">';
	$html.= '<div id="swiper-carousel-' . $uniqid . '" ' . $data_attr_line . '>';
	$html.= '<ul class="swiper-wrapper">';

	$allowed_html = array(
		'div' => array(
			'data-uniq-id'              => true,
			'data-slides-per-view'      => true,
			'data-slides-per-group'     => true,
			'data-slides-per-column'    => true,
			'data-space-between-slides' => true,
			'data-duration-speed'       => true,
			'data-swiper-loop'          => true,
			'data-free-mode'            => true,
			'data-grab-cursor'          => true,
			'data-mouse-wheel'          => true,
		),
	);

	if ( true === $echo ) {
		echo wp_kses( $html, storycle_kses_post_allowed_html( $allowed_html ) );
	}else{
		return wp_kses( $html, storycle_kses_post_allowed_html( $allowed_html ) );
	}
}

/**
 * Closed wrapper for carousel loop products
 *
 * @param bool $echo
 * @param bool $swiper
 *
 * @return string
 */
function storycle_woocommerce_product_loop_carousel_end( $echo = true, $swiper = false ) {

	global $uniqid;

	$html = '</ul>';
	$html.= '<div id="swiper-carousel-' . $uniqid . '-next" class="swiper-button-next button-next"></div><div id="swiper-carousel-' . $uniqid . '-prev" class="swiper-button-prev button-prev"></div>';
	$html.= '</div>';
	$html.= '</div>';

	unset( $GLOBALS['uniqid'] );

	if ( true === $echo ) {
		echo wp_kses_post( $html );
	} else{
		return wp_kses_post( $html );
	}
}

/**
 * Cart item name.
 *
 * @param $title
 * @param $cart_item
 * @param $cart_item_key
 *
 * @return string
 */
function storycle_woocommerce_cart_item_name( $title, $cart_item, $cart_item_key ) {
	// Split on spaces.
	$title = preg_split( '/\s+/', $title );

	// Slice array
	$title = array_slice( $title, 0, 4 );

	// Re-create the string.
	$title = join( ' ', $title );

	return '<span class="mini_cart_item_title">' . $title . '</span>';
}

/**
 * Function for Smartbox thumbnail
 */
function storycle_woocommerce_template_loop_product_thumbnail_custom_size() {
	echo woocommerce_get_product_thumbnail( 'storycle-smart-box-thumb' );
}

function storycle_products_smart_box_widget__cat_img_size() {
	return 'storycle-thumb-listing-line-product';
}

/**
 * Remove woo-pagination and added theme pagination
 */
function storycle_woocommerce_pagination_args( $args = array() ) {

	unset( $args[ 'type' ] );
	unset( $args[ 'end_size' ] );
	unset( $args[ 'mid_size' ] );

	return $args;
}

/**
 * Add product categories in product list
 */
function storycle_woocommerce_list_categories() {
	$sep    = '</li> <li>';
	$before = '<ul class="product-categories"><li>';
	$after  = '</li></ul>';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0', '>=' ) ) {
		echo wc_get_product_category_list( get_the_id(), $sep, $before, $after );
	} else {
		global $product;

		if ( ! empty( $product ) ) {
			echo wp_kses_post( $product->get_categories( $sep, $before, $after ) );
		}
	}
}

/**
 * Display short description for listing-line template product
 *
 */
function storycle_woocommerce_display_short_excerpt( $atts = '' ) {
	if ( 'list' === $atts ) {
		echo '<div class="desc_products_listing_line">';
		woocommerce_template_single_excerpt( $atts );
		echo '</div>';
	}
}

function storycle_woocommerce_list_tag(){
	global $product;
	echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'storycle' ) . ' ', '</span>' );
}

/**
 * Add layout synchronization for product loop and product carousel widget.
 *
 */
function storycle_products_carousel_widget_hooks( $hooks ) {

	$hooks[ 'cat' ] = array(
		'woocommerce_before_shop_loop_item_title',
		'storycle_woocommerce_list_categories',
		14,
		1
	);

	$hooks[ 'tag' ] = array(
		'woocommerce_before_shop_loop_item_title',
		'storycle_woocommerce_list_tag',
		14,
		1
	);

	$hooks[ 'title' ] = array(
		'woocommerce_shop_loop_item_title',
		'storycle_template_loop_product_title',
		10,
		1
	);

	$hooks[ 'price' ] = array(
		'woocommerce_after_shop_loop_item',
		'woocommerce_template_loop_price',
		5,
		1
	);

	return $hooks;
}

add_filter( 'tm_wc_ahax_filters_loader', 'storycle_wc_compare_wishlist_ahax_filters_loader' );
add_filter( 'tm_wc_compare_wishlist_loader', 'storycle_wc_compare_wishlist_ahax_filters_loader' );

function storycle_wc_compare_wishlist_ahax_filters_loader(){
	return '<span></span>';
}

/**
 * Replace default breadcrubs trail with WooCommerce-related.
 *
 * @param  bool  $is_custom_breadcrumbs Default cutom breadcrumbs trigger.
 * @param  array $args                  Breadcrumb arguments.
 * @return bool|array
 */
function storycle_get_woo_breadcrumbs( $is_custom_breadcrumbs, $args ) {

	if ( ! function_exists( 'is_woocommerce' ) || ! is_woocommerce() ) {
		return false;
	}

	if ( ! class_exists( 'Storycle_Woo_Breadcrumbs' ) ) {
		require_once get_parent_theme_file_path( 'inc/classes/class-woocommerce-breadcrumbs.php' );
	}

	$woo_breadcrums = new Storycle_Woo_Breadcrumbs( storycle_theme()->get_core(), $args );

	return array( 'items' => $woo_breadcrums->items, 'page_title' => $woo_breadcrums->page_title );

}

/**
 * Check availability tm_wc_ajax - plugin
 *
 * @return bool
 */
function storycle_is_ajax_shop() {
	if ( function_exists( 'tm_wc_ajax_is_shop' ) ) {
		return tm_wc_ajax_is_shop();
	} else {
		return false;
	}
}
