<?php
/**
 * Footer template part.
 *
 * @package Storycle
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */
?>
<footer class="amp-wp-footer">
	<div>
		<?php storycle_footer_copyright(); ?>
		<a href="#top" class="back-to-top"><?php esc_html_e( 'Back to top', 'storycle' ); ?></a>
	</div>
</footer>
