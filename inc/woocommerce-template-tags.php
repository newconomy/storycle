<?php
/**
 * Cart Link
 * Displayed a link to the cart including the number of items present and the cart total
 *
 * @since  1.0.0
 */
function storycle_cart_link() {
	?>
		<div class="cart-contents" title="<?php esc_html_e( 'View your shopping cart', 'storycle' ); ?>">
			<i class="fa fa-shopping-cart"></i>
			<span class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
		</div>
	<?php
}

/**
 * Cart Link Fragment.
 *
 * @param $fragments
 *
 * @return mixed
 */
function storycle_cart_link_fragment( $fragments ) {
	global $woocommerce;
	ob_start();
	storycle_cart_link();
	$fragments['div.cart-contents'] = ob_get_clean();
	return $fragments;
}

/**
 * Display Header Cart
 *
 * @since  1.0.0
 * @return void
 */
function storycle_header_cart() {
	if ( !is_cart() && !is_checkout() ) {
		get_template_part( 'woocommerce/header-cart' );
	}
}
