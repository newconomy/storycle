<?php
/**
 * Theme Customizer.
 *
 * @package Storycle
 */

/**
 * Retrieve a holder for Customizer options.
 *
 * @since  1.0.0
 * @return array
 */
function storycle_get_customizer_options() {
	/**
	 * Filter a holder for Customizer options (for theme/plugin developer customization).
	 *
	 * @since 1.0.0
	 */
	return apply_filters( 'storycle_get_customizer_options', array(
		'prefix'     => 'storycle',
		'capability' => 'edit_theme_options',
		'type'       => 'theme_mod',
		'options'    => array(

			/** `Site Identity` section */
			'show_tagline' => array(
				'title'    => esc_html__( 'Show tagline after logo', 'storycle' ),
				'section'  => 'title_tagline',
				'priority' => 60,
				'default'  => false,
				'field'    => 'checkbox',
				'type'     => 'control',
			),
			'totop_visibility' => array(
				'title'    => esc_html__( 'Show ToTop button', 'storycle' ),
				'section'  => 'title_tagline',
				'priority' => 61,
				'default'  => true,
				'field'    => 'checkbox',
				'type'     => 'control',
			),

			/** `General Site settings` panel */
			'general_settings' => array(
				'title'    => esc_html__( 'General Site settings', 'storycle' ),
				'priority' => 40,
				'type'     => 'panel',
			),

			/** `Logo & Favicon` section */
			'logo_favicon' => array(
				'title'    => esc_html__( 'Logo &amp; Favicon', 'storycle' ),
				'priority' => 25,
				'panel'    => 'general_settings',
				'type'     => 'section',
			),

			/** `Breadcrumbs` section */
			'breadcrumbs' => array(
				'title'    => esc_html__( 'Breadcrumbs', 'storycle' ),
				'priority' => 30,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'breadcrumbs_visibillity' => array(
				'title'   => esc_html__( 'Enable Breadcrumbs', 'storycle' ),
				'section' => 'breadcrumbs',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_front_visibillity' => array(
				'title'   => esc_html__( 'Enable Breadcrumbs on front page', 'storycle' ),
				'section' => 'breadcrumbs',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_page_title' => array(
				'title'   => esc_html__( 'Enable page title in breadcrumbs area', 'storycle' ),
				'section' => 'breadcrumbs',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'breadcrumbs_path_type' => array(
				'title'   => esc_html__( 'Show full/minified path', 'storycle' ),
				'section' => 'breadcrumbs',
				'default' => 'full',
				'field'   => 'select',
				'choices' => array(
					'full'     => esc_html__( 'Full', 'storycle' ),
					'minified' => esc_html__( 'Minified', 'storycle' ),
				),
				'type'    => 'control',
			),

			/** `Page Layout` section */
			'page_layout' => array(
				'title'    => esc_html__( 'Page Layout', 'storycle' ),
				'priority' => 55,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'page_layout_style' => array(
				'title'   => esc_html__( 'Page Layout Style', 'storycle' ),
				'section' => 'page_layout',
				'default' => 'fullwidth',
				'field'   => 'select',
				'choices' => array(
					'boxed'     => esc_html__( 'Boxed', 'storycle' ),
					'framed'    => esc_html__( 'Framed', 'storycle' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'storycle' ),
				),
				'type'    => 'control',
			),
			'page_boxed_width' => array(
				'title'       => esc_html__( 'Page width (px)', 'storycle' ),
				'section'     => 'page_layout',
				'default'     => 1230,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 960,
					'max'  => 1920,
					'step' => 1,
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_page_type_boxed',
			),
			'page_boxed_bg_color' => array(
				'title'           => esc_html__( 'Page Inner Background', 'storycle' ),
				'section'         => 'page_layout',
				'default'         => '#ffffff',
				'field'           => 'hex_color',
				'type'            => 'control',
				'active_callback' => 'storycle_is_page_type_boxed',
			),
			'container_width' => array(
				'title'       => esc_html__( 'Container width (px)', 'storycle' ),
				'section'     => 'page_layout',
				'default'     => 1200,
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 960,
					'max'  => 1920,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'sidebar_width' => array(
				'title'   => esc_html__( 'Sidebar width', 'storycle' ),
				'section' => 'page_layout',
				'default' => '1/3',
				'field'   => 'select',
				'choices' => array(
					'1/3' => '1/3',
					'1/4' => '1/4',
				),
				'sanitize_callback' => 'sanitize_text_field',
				'type'              => 'control',
			),

			/** `Page Preloader` section */
			'page_preloader_section' => array(
				'title'    => esc_html__( 'Page Preloader', 'storycle' ),
				'priority' => 60,
				'type'     => 'section',
				'panel'    => 'general_settings',
			),
			'page_preloader' => array(
				'title'    => esc_html__( 'Show page preloader', 'storycle' ),
				'section'  => 'page_preloader_section',
				'priority' => 10,
				'default'  => true,
				'field'    => 'checkbox',
				'type'     => 'control',
			),
			'page_preloader_bg' => array(
				'title'           => esc_html__( 'Preloader Cover Background Color', 'storycle' ),
				'section'         => 'page_preloader_section',
				'default'         => '#ffffff',
				'field'           => 'hex_color',
				'type'            => 'control',
				'active_callback' => 'storycle_is_page_preloader_enable',
			),
			'page_preloader_url' => array(
				'title'           => esc_html__( 'Preloader Image Upload', 'storycle' ),
				'section'         => 'page_preloader_section',
				'field'           => 'image',
				'default'         => '%s/assets/images/preloader-image.png',
				'type'            => 'control',
				'active_callback' => 'storycle_is_page_preloader_enable',
			),
			'page_preloader_url_retina' => array(
				'title'           => esc_html__( 'Preloader Retina Image Upload', 'storycle' ),
				'section'         => 'page_preloader_section',
				'field'           => 'image',
				'default'         => '%s/assets/images/preloader-image-retina.png',
				'type'            => 'control',
				'active_callback' => 'storycle_is_page_preloader_enable',
			),

			/** `Color Scheme` panel */
			'color_scheme' => array(
				'title'       => esc_html__( 'Color Scheme', 'storycle' ),
				'description' => esc_html__( 'Configure Color Scheme', 'storycle' ),
				'priority'    => 40,
				'type'        => 'panel',
			),

			/** `Regular scheme` section */
			'regular_scheme' => array(
				'title'    => esc_html__( 'Regular scheme', 'storycle' ),
				'priority' => 10,
				'panel'    => 'color_scheme',
				'type'     => 'section',
			),
			'regular_accent_color_1' => array(
				'title'   => esc_html__( 'Accent color (1)', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#377eff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_accent_color_2' => array(
				'title'   => esc_html__( 'Accent color (2)', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#00c8ec',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_text_color' => array(
				'title'   => esc_html__( 'Text color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_link_color' => array(
				'title'   => esc_html__( 'Link color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#377eff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_link_hover_color' => array(
				'title'   => esc_html__( 'Link hover color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#00c8ec',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h1_color' => array(
				'title'   => esc_html__( 'H1 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h2_color' => array(
				'title'   => esc_html__( 'H2 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h3_color' => array(
				'title'   => esc_html__( 'H3 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h4_color' => array(
				'title'   => esc_html__( 'H4 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h5_color' => array(
				'title'   => esc_html__( 'H5 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'regular_h6_color' => array(
				'title'   => esc_html__( 'H6 color', 'storycle' ),
				'section' => 'regular_scheme',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Additional colors` section */
			'additional_colors' => array(
				'title'    => esc_html__( 'Additional colors', 'storycle' ),
				'priority' => 30,
				'panel'    => 'color_scheme',
				'type'     => 'section',
			),
			'light_color' => array(
				'title'   => esc_html__( 'Light color', 'storycle' ),
				'section' => 'additional_colors',
				'default' => '#ffffff',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'grey_color_1' => array(
				'title'   => esc_html__( 'Grey color (1)', 'storycle' ),
				'section' => 'additional_colors',
				'default' => '#c3c3c9',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'dark_color' => array(
				'title'   => esc_html__( 'Dark color', 'storycle' ),
				'section' => 'additional_colors',
				'default' => '#29293a',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Typography Settings` panel */
			'typography' => array(
				'title'       => esc_html__( 'Typography', 'storycle' ),
				'description' => esc_html__( 'Configure typography settings', 'storycle' ),
				'priority'    => 50,
				'type'        => 'panel',
			),

			/** `Logo` section */
			'header_logo_typography' => array(
				'title'    => esc_html__( 'Logo', 'storycle' ),
				'priority' => 5,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'header_logo_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'header_logo_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'header_logo_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'header_logo_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'header_logo_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'header_logo_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'header_logo_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'header_logo_typography',
				'default'     => '30',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'header_logo_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'header_logo_typography',
				'default'     => '1',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'header_logo_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'header_logo_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'header_logo_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'header_logo_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'header_logo_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'header_logo_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `Body text` section */
			'body_typography' => array(
				'title'    => esc_html__( 'Body text', 'storycle' ),
				'priority' => 5,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'body_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'body_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'body_font_style_italic' => array(
				'title'   => esc_html__( 'Additional Font Style', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'italic',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
				'active_callback' => '__return_false',
			),
			'body_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'body_typography',
				'default' => '300',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'body_font_weight_bold' => array(
				'title'   => esc_html__( 'Additional Font Weight', 'storycle' ),
				'section' => 'body_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
				'active_callback' => '__return_false',
			),
			'body_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'body_typography',
				'default'     => '15',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'body_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'body_typography',
				'default'     => '1.6',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'body_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'body_typography',
				'default'     => '0.03',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'body_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'body_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'left',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'body_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'body_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H1 Heading` section */
			'h1_typography' => array(
				'title'    => esc_html__( 'H1 Heading', 'storycle' ),
				'priority' => 10,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h1_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h1_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h1_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h1_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h1_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h1_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h1_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h1_typography',
				'default'     => '44',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h1_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h1_typography',
				'default'     => '1.2',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h1_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h1_typography',
				'default'     => '-0.02',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h1_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h1_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h1_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h1_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h1_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h1_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H2 Heading` section */
			'h2_typography' => array(
				'title'    => esc_html__( 'H2 Heading', 'storycle' ),
				'priority' => 15,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h2_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h2_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h2_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h2_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h2_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h2_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h2_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h2_typography',
				'default'     => '32',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h2_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h2_typography',
				'default'     => '1.25',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h2_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h2_typography',
				'default'     => '-0.02',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h2_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h2_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h2_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h2_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h2_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h2_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H3 Heading` section */
			'h3_typography' => array(
				'title'    => esc_html__( 'H3 Heading', 'storycle' ),
				'priority' => 20,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h3_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h3_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h3_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h3_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h3_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h3_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h3_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h3_typography',
				'default'     => '26',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h3_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h3_typography',
				'default'     => '1.3',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h3_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h3_typography',
				'default'     => '-0.02',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h3_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h3_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h3_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h3_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h3_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h3_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H4 Heading` section */
			'h4_typography' => array(
				'title'    => esc_html__( 'H4 Heading', 'storycle' ),
				'priority' => 25,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h4_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h4_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h4_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h4_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h4_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h4_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h4_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h4_typography',
				'default'     => '22',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h4_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h4_typography',
				'default'     => '1.36',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h4_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h4_typography',
				'default'     => '-0.02',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h4_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h4_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h4_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h4_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h4_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h4_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H5 Heading` section */
			'h5_typography' => array(
				'title'    => esc_html__( 'H5 Heading', 'storycle' ),
				'priority' => 30,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h5_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h5_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h5_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h5_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h5_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h5_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h5_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h5_typography',
				'default'     => '18',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h5_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h5_typography',
				'default'     => '1.33',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h5_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h5_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h5_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h5_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h5_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h5_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h5_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h5_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `H6 Heading` section */
			'h6_typography' => array(
				'title'    => esc_html__( 'H6 Heading', 'storycle' ),
				'priority' => 35,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'h6_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'h6_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'h6_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'h6_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'h6_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'h6_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'h6_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'h6_typography',
				'default'     => '14',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 200,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'h6_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'h6_typography',
				'default'     => '1.4',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'h6_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'h6_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'h6_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'h6_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'h6_text_align' => array(
				'title'   => esc_html__( 'Text Align', 'storycle' ),
				'section' => 'h6_typography',
				'default' => 'inherit',
				'field'   => 'select',
				'choices' => storycle_get_text_aligns(),
				'type'    => 'control',
			),
			'h6_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'h6_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `Custom Font Style` section */
			'custom_typography' => array(
				'title'       => esc_html__( 'Custom Font Style', 'storycle' ),
				'description' => esc_html__( 'Used in typography of buttons, categories, etc.', 'storycle' ),
				'priority'    => 45,
				'panel'       => 'typography',
				'type'        => 'section',
			),
			'custom_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'custom_typography',
				'default' => 'Roboto Condensed, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'custom_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'custom_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'custom_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'custom_typography',
				'default' => '700',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'custom_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'custom_typography',
				'default'     => '0.02',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'custom_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'custom_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),

			/** `Breadcrumbs` section */
			'breadcrumbs_typography' => array(
				'title'    => esc_html__( 'Breadcrumbs', 'storycle' ),
				'priority' => 45,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'breadcrumbs_font_family' => array(
				'title'   => esc_html__( 'Font Family', 'storycle' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'Roboto, sans-serif',
				'field'   => 'fonts',
				'type'    => 'control',
			),
			'breadcrumbs_font_style' => array(
				'title'   => esc_html__( 'Font Style', 'storycle' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'normal',
				'field'   => 'select',
				'choices' => storycle_get_font_styles(),
				'type'    => 'control',
			),
			'breadcrumbs_font_weight' => array(
				'title'   => esc_html__( 'Font Weight', 'storycle' ),
				'section' => 'breadcrumbs_typography',
				'default' => '300',
				'field'   => 'select',
				'choices' => storycle_get_font_weight(),
				'type'    => 'control',
			),
			'breadcrumbs_font_size' => array(
				'title'       => esc_html__( 'Font Size, px', 'storycle' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '12',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 6,
					'max'  => 50,
					'step' => 1,
				),
				'type'        => 'control',
			),
			'breadcrumbs_line_height' => array(
				'title'       => esc_html__( 'Line Height', 'storycle' ),
				'description' => esc_html__( 'Relative to the font-size of the element', 'storycle' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '1.5',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => 1.0,
					'max'  => 3.0,
					'step' => 0.1,
				),
				'type'        => 'control',
			),
			'breadcrumbs_letter_spacing' => array(
				'title'       => esc_html__( 'Letter Spacing, em', 'storycle' ),
				'section'     => 'breadcrumbs_typography',
				'default'     => '0',
				'field'       => 'number',
				'input_attrs' => array(
					'min'  => - 1,
					'max'  => 1,
					'step' => 0.01,
				),
				'type'        => 'control',
			),
			'breadcrumbs_character_set' => array(
				'title'   => esc_html__( 'Character Set', 'storycle' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'latin',
				'field'   => 'select',
				'choices' => storycle_get_character_sets(),
				'type'    => 'control',
			),
			'breadcrumbs_text_transform' => array(
				'title'   => esc_html__( 'Text Transform', 'storycle' ),
				'section' => 'breadcrumbs_typography',
				'default' => 'none',
				'field'   => 'select',
				'choices' => storycle_get_text_transform(),
				'type'    => 'control',
			),

			/** `Typography misc` section */
			'misc_styles' => array(
				'title'    => esc_html__( 'Misc', 'storycle' ),
				'priority' => 60,
				'panel'    => 'typography',
				'type'     => 'section',
			),
			'word_wrap' => array(
				'title'   => esc_html__( 'Enable Word Wrap', 'storycle' ),
				'section' => 'misc_styles',
				'default' => false,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Header` panel */
			'header_options' => array(
				'title'    => esc_html__( 'Header', 'storycle' ),
				'priority' => 60,
				'type'     => 'panel',
			),

			/** `Header styles` section */
			'header_styles' => array(
				'title'    => esc_html__( 'Header Styles', 'storycle' ),
				'priority' => 5,
				'panel'    => 'header_options',
				'type'     => 'section',
			),
			'header_bg_transparent' => array(
				'title'   => esc_html__( 'Background Transparent', 'storycle' ),
				'section' => 'header_styles',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'header_bg_color' => array(
				'title'           => esc_html__( 'Background Color', 'storycle' ),
				'section'         => 'header_styles',
				'field'           => 'hex_color',
				'default'         => '#ffffff',
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),
			'header_bg_image' => array(
				'title'           => esc_html__( 'Background Image', 'storycle' ),
				'section'         => 'header_styles',
				'field'           => 'image',
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),
			'header_bg_repeat' => array(
				'title'           => esc_html__( 'Background Repeat', 'storycle' ),
				'section'         => 'header_styles',
				'default'         => 'no-repeat',
				'field'           => 'select',
				'choices'         => storycle_get_bg_repeat(),
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),
			'header_bg_position' => array(
				'title'           => esc_html__( 'Background Position', 'storycle' ),
				'section'         => 'header_styles',
				'default'         => 'center',
				'field'           => 'select',
				'choices'         => storycle_get_bg_position(),
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),
			'header_bg_size' => array(
				'title'           => esc_html__( 'Background Size', 'storycle' ),
				'section'         => 'header_styles',
				'default'         => 'cover',
				'field'           => 'select',
				'choices'         => storycle_get_bg_size(),
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),
			'header_bg_attachment' => array(
				'title'           => esc_html__( 'Background Attachment', 'storycle' ),
				'section'         => 'header_styles',
				'default'         => 'scroll',
				'field'           => 'select',
				'choices'         => storycle_get_bg_attachment(),
				'type'            => 'control',
				'active_callback' => 'storycle_is_not_header_bg_transparent',
			),

			/** `Sidebar` section */
			'sidebar_settings' => array(
				'title'    => esc_html__( 'Sidebar', 'storycle' ),
				'priority' => 105,
				'type'     => 'section',
			),
			'sidebar_position' => array(
				'title'   => esc_html__( 'Sidebar Position', 'storycle' ),
				'section' => 'sidebar_settings',
				'default' => 'one-right-sidebar',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'storycle' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'storycle' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'storycle' ),
				),
				'type'    => 'control',
			),
			'sidebar_position_post' => array(
				'title'   => esc_html__( 'Sidebar Position on Blog Post', 'storycle' ),
				'section' => 'sidebar_settings',
				'default' => 'one-right-sidebar',
				'field'   => 'select',
				'choices' => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'storycle' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'storycle' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'storycle' ),
				),
				'type'    => 'control',
			),
			'sidebar_position_product' => array(
				'title'           => esc_html__( 'Sidebar Position on Single Product', 'storycle' ),
				'section'         => 'sidebar_settings',
				'default'         => 'one-left-sidebar',
				'field'           => 'select',
				'choices'         => array(
					'one-left-sidebar'  => esc_html__( 'Sidebar on left side', 'storycle' ),
					'one-right-sidebar' => esc_html__( 'Sidebar on right side', 'storycle' ),
					'fullwidth'         => esc_html__( 'No sidebars', 'storycle' ),
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_woocommerce_activated',
			),
			'sidebar_sticky' => array(
				'title'   => esc_html__( 'Enable sticky sidebar', 'storycle' ),
				'section' => 'sidebar_settings',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Ads Management` panel */
			'ads_management' => array(
				'title'    => esc_html__( 'Ads Management', 'storycle' ),
				'priority' => 110,
				'type'     => 'section',
			),
			'ads_header' => array(
				'title'             => esc_html__( 'Header', 'storycle' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_home_before_loop' => array(
				'title'             => esc_html__( 'Posts Page Before Loop', 'storycle' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_post_before_content' => array(
				'title'             => esc_html__( 'Post Before Content', 'storycle' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),
			'ads_post_before_comments' => array(
				'title'             => esc_html__( 'Post Before Comments', 'storycle' ),
				'section'           => 'ads_management',
				'field'             => 'textarea',
				'default'           => '',
				'sanitize_callback' => 'esc_html',
				'type'              => 'control',
			),

			/** `Footer` panel */
			'footer_options' => array(
				'title'    => esc_html__( 'Footer', 'storycle' ),
				'priority' => 110,
				'type'     => 'panel',
			),

			/** `Footer styles` section */
			'footer_styles' => array(
				'title'    => esc_html__( 'Footer Styles', 'storycle' ),
				'priority' => 5,
				'panel'    => 'footer_options',
				'type'     => 'section',
			),
			'footer_copyright' => array(
				'title'   => esc_html__( 'Copyright text', 'storycle' ),
				'section' => 'footer_styles',
				'default' => storycle_get_default_footer_copyright(),
				'field'   => 'textarea',
				'type'    => 'control',
			),
			'footer_bg' => array(
				'title'   => esc_html__( 'Footer Background color', 'storycle' ),
				'section' => 'footer_styles',
				'default' => '#1c1c21',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'footer_text_color' => array(
				'title'   => esc_html__( 'Footer color', 'storycle' ),
				'section' => 'footer_styles',
				'default' => '#c3c3c9',
				'field'   => 'hex_color',
				'type'    => 'control',
			),

			/** `Blog Settings` panel */
			'blog_settings' => array(
				'title'    => esc_html__( 'Blog Settings', 'storycle' ),
				'priority' => 115,
				'type'     => 'panel',
			),

			/** `Blog` section */
			'blog' => array(
				'title'    => esc_html__( 'Blog', 'storycle' ),
				'panel'    => 'blog_settings',
				'priority' => 10,
				'type'     => 'section',
			),
			'blog_layout_type' => array(
				'title'   => esc_html__( 'Layout', 'storycle' ),
				'section' => 'blog',
				'default' => 'default',
				'field'   => 'select',
				'choices' => storycle_get_blog_layouts(),
				'type'    => 'control',
			),
			'blog_layout_columns' => array(
				'title'           => esc_html__( 'Columns', 'storycle' ),
				'section'         => 'blog',
				'default'         => '2-cols',
				'field'           => 'select',
				'choices'         => array(
					'2-cols' => esc_html__( '2 columns', 'storycle' ),
					'3-cols' => esc_html__( '3 columns', 'storycle' ),
					'4-cols' => esc_html__( '4 columns', 'storycle' ),
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_blog_layout_type_grid_masonry',
			),
			'blog_pagination_type' => array(
				'title'   => esc_html__( 'Pagination Type', 'storycle' ),
				'section' => 'blog',
				'default' => 'load-more',
				'field'   => 'select',
				'choices' => array(
					'default'   => esc_html__( 'Default', 'storycle' ),
					'load-more' => esc_html__( 'Load More', 'storycle' ),
				),
				'type'    => 'control',
			),
			'blog_sticky_type' => array(
				'title'   => esc_html__( 'Sticky label type', 'storycle' ),
				'section' => 'blog',
				'default' => 'icon',
				'field'   => 'select',
				'choices' => array(
					'label' => esc_html__( 'Text Label', 'storycle' ),
					'icon'  => esc_html__( 'Font Icon', 'storycle' ),
					'both'  => esc_html__( 'Text with Icon', 'storycle' ),
				),
				'type'    => 'control',
			),
			'blog_sticky_icon' => array(
				'title'           => esc_html__( 'Icon for sticky post', 'storycle' ),
				'section'         => 'blog',
				'field'           => 'iconpicker',
				'default'         => 'fa-star-o',
				'icon_data'       => storycle_get_fa_icons_data(),
				'type'            => 'control',
				'transport'       => 'postMessage',
				'active_callback' => 'storycle_is_sticky_icon',
			),
			'blog_sticky_label' => array(
				'title'           => esc_html__( 'Featured Post Label', 'storycle' ),
				'description'     => esc_html__( 'Label for sticky post', 'storycle' ),
				'section'         => 'blog',
				'default'         => esc_html__( 'Featured', 'storycle' ),
				'field'           => 'text',
				'active_callback' => 'storycle_is_sticky_text',
				'type'            => 'control',
				'transport'       => 'postMessage',
			),
			'blog_posts_content' => array(
				'title'   => esc_html__( 'Post content', 'storycle' ),
				'section' => 'blog',
				'default' => 'excerpt',
				'field'   => 'select',
				'choices' => array(
					'excerpt' => esc_html__( 'Only excerpt', 'storycle' ),
					'full'    => esc_html__( 'Full content', 'storycle' ),
					'none'    => esc_html__( 'Hide', 'storycle' ),
				),
				'type'    => 'control',
			),
			'blog_posts_content_length' => array(
				'title'           => esc_html__( 'Number of words in the excerpt', 'storycle' ),
				'section'         => 'blog',
				'default'         => '30',
				'field'           => 'number',
				'input_attrs'     => array(
					'min'  => 1,
					'max'  => 100,
					'step' => 1,
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_blog_posts_content_type_excerpt',
			),
			'blog_read_more_btn' => array(
				'title'   => esc_html__( 'Show Read More button', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_read_more_text' => array(
				'title'           => esc_html__( 'Read More button text', 'storycle' ),
				'section'         => 'blog',
				'default'         => esc_html__( 'Read more', 'storycle' ),
				'field'           => 'text',
				'type'            => 'control',
				'transport'       => 'postMessage',
				'active_callback' => 'storycle_is_blog_read_more_btn_enable',
			),
			'blog_post_author' => array(
				'title'   => esc_html__( 'Show post author', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_publish_date' => array(
				'title'   => esc_html__( 'Show publish date', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_categories' => array(
				'title'   => esc_html__( 'Show categories', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_tags'  => array(
				'title'   => esc_html__( 'Show tags', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_comments' => array(
				'title'   => esc_html__( 'Show comments', 'storycle' ),
				'section' => 'blog',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'blog_post_trend_views' => array(
				'title'           => esc_html__( 'Show views counter', 'storycle' ),
				'section'         => 'blog',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_trending_posts_activated',
			),
			'blog_post_trend_rating' => array(
				'title'           => esc_html__( 'Show rating', 'storycle' ),
				'section'         => 'blog',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_trending_posts_activated',
			),
			'blog_post_share_buttons' => array(
				'title'           => esc_html__( 'Show social sharing buttons', 'storycle' ),
				'section'         => 'blog',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_socialize_activated',
			),

			/** `Post` section */
			'blog_post' => array(
				'title'           => esc_html__( 'Post', 'storycle' ),
				'panel'           => 'blog_settings',
				'priority'        => 20,
				'type'            => 'section',
				'active_callback' => 'callback_single',
			),
			'single_post_author' => array(
				'title'   => esc_html__( 'Show post author', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_publish_date' => array(
				'title'   => esc_html__( 'Show publish date', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_categories' => array(
				'title'   => esc_html__( 'Show categories', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_tags' => array(
				'title'   => esc_html__( 'Show tags', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_comments' => array(
				'title'   => esc_html__( 'Show comments', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_reading_time' => array(
				'title'   => esc_html__( 'Show reading time', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_trend_views' => array(
				'title'           => esc_html__( 'Show views counter', 'storycle' ),
				'section'         => 'blog_post',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_trending_posts_activated',
			),
			'single_post_trend_rating' => array(
				'title'           => esc_html__( 'Show rating', 'storycle' ),
				'section'         => 'blog_post',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_trending_posts_activated',
			),
			'single_post_share_buttons' => array(
				'title'           => esc_html__( 'Show social sharing buttons', 'storycle' ),
				'section'         => 'blog_post',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_cherry_socialize_activated',
			),
			'single_post_sources' => array(
				'title'   => esc_html__( 'Show source', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_via' => array(
				'title'   => esc_html__( 'Show via', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_respond_button' => array(
				'title'   => esc_html__( 'Show the respond button after post content', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_author_block' => array(
				'title'   => esc_html__( 'Show the author block after each post', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_navigation' => array(
				'title'   => esc_html__( 'Show post navigation', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'single_post_reading_progress' => array(
				'title'   => esc_html__( 'Show reading progress bar', 'storycle' ),
				'section' => 'blog_post',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),

			/** `Related Posts` section */
			'related_posts' => array(
				'title'           => esc_html__( 'Related posts block', 'storycle' ),
				'panel'           => 'blog_settings',
				'priority'        => 30,
				'type'            => 'section',
				'active_callback' => 'callback_single',
			),
			'related_posts_visible' => array(
				'title'   => esc_html__( 'Show related posts block', 'storycle' ),
				'section' => 'related_posts',
				'default' => true,
				'field'   => 'checkbox',
				'type'    => 'control',
			),
			'related_posts_block_title' => array(
				'title'           => esc_html__( 'Related posts block title', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => esc_html__( 'Other Posts', 'storycle' ),
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
				'transport'       => 'postMessage',
			),
			'related_posts_query_type' => array(
				'title'           => esc_html__( 'Related posts query type', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => 'post_tag',
				'field'           => 'select',
				'choices'         => array(
					'category' => esc_html__( 'Categories', 'storycle' ),
					'post_tag' => esc_html__( 'Tags', 'storycle' ),
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_count' => array(
				'title'           => esc_html__( 'Number of post', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => '8',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_grid' => array(
				'title'           => esc_html__( 'Layout', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => '2',
				'field'           => 'select',
				'choices'         => array(
					'2' => esc_html__( '2 columns', 'storycle' ),
					'3' => esc_html__( '3 columns', 'storycle' ),
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_title_length' => array(
				'title'           => esc_html__( 'Number of words in the title', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => '10',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_content' => array(
				'title'           => esc_html__( 'Display content', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => 'hide',
				'field'           => 'select',
				'choices'         => array(
					'hide'         => esc_html__( 'Hide', 'storycle' ),
					'post_excerpt' => esc_html__( 'Excerpt', 'storycle' ),
					'post_content' => esc_html__( 'Content', 'storycle' ),
				),
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_content_length' => array(
				'title'           => esc_html__( 'Number of words in the content', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => '10',
				'field'           => 'text',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_author' => array(
				'title'           => esc_html__( 'Show post author', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_publish_date' => array(
				'title'           => esc_html__( 'Show post publish date', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => true,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),
			'related_posts_comment_count' => array(
				'title'           => esc_html__( 'Show post comment count', 'storycle' ),
				'section'         => 'related_posts',
				'default'         => false,
				'field'           => 'checkbox',
				'type'            => 'control',
				'active_callback' => 'storycle_is_related_posts_enable',
			),

			/** `Badge` controls */
			'onsale_badge_bg' => array(
				'title'   => esc_html__( 'Onsale badge background', 'storycle' ),
				'section' => 'woocommerce_product_catalog',
				'default' => '#ff3a4c',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'featured_badge_bg' => array(
				'title'   => esc_html__( 'Featured badge background', 'storycle' ),
				'section' => 'woocommerce_product_catalog',
				'default' => '#ffeb3b',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
			'new_badge_bg' => array(
				'title'   => esc_html__( 'New badge background', 'storycle' ),
				'section' => 'woocommerce_product_catalog',
				'default' => '#04e2f6',
				'field'   => 'hex_color',
				'type'    => 'control',
			),
		),
	) );
}

/**
 * Return true if setting is value. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @param  string $setting Setting name to check.
 * @param  string $value   Setting value to compare.
 * @return bool
 */
function storycle_is_setting( $control, $setting, $value ) {

	if ( $value == $control->manager->get_setting( $setting )->value() ) {
		return true;
	}

	return false;
}

/**
 * Return true if value of passed setting is not equal with passed value.
 *
 * @param  object $control Parent control.
 * @param  string $setting Setting name to check.
 * @param  string $value   Setting value to compare.
 * @return bool
 */
function storycle_is_not_setting( $control, $setting, $value ) {

	if ( $value !== $control->manager->get_setting( $setting )->value() ) {
		return true;
	}

	return false;
}

/**
 * Return true if sticky label type set to text or text with icon.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_sticky_text( $control ) {
	return storycle_is_not_setting( $control, 'blog_sticky_type', 'icon' );
}

/**
 * Return true if sticky label type set to icon or text with icon.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_sticky_icon( $control ) {
	return storycle_is_not_setting( $control, 'blog_sticky_type', 'label' );
}

/**
 * Return true if option `Show Related Posts Block` is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_related_posts_enable( $control ) {
	return storycle_is_setting( $control, 'related_posts_visible', true );
}

/**
 * Return true if option `Blog posts content` is excerpt. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_blog_posts_content_type_excerpt( $control ) {
	return storycle_is_setting( $control, 'blog_posts_content', 'excerpt' );
}

/**
 * Return true if option `Show Read More button` is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_blog_read_more_btn_enable( $control ) {
	return storycle_is_setting( $control, 'blog_read_more_btn', true );
}

/**
 * Return true if `Blog layout` selected Grid or Masonry. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_blog_layout_type_grid_masonry( $control ) {
	if ( in_array( $control->manager->get_setting( 'blog_layout_type' )->value(), array( 'grid', 'grid-2', 'masonry' ) ) ) {
		return true;
	}

	return false;
}

/**
 * Return true if option `Show Page Preloader` is enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_page_preloader_enable( $control ) {
	return storycle_is_setting( $control, 'page_preloader', true );
}

/**
 * Return true if option `Page type` is boxed or framed. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_page_type_boxed( $control ) {
	if ( in_array( $control->manager->get_setting( 'page_layout_style' )->value(), array( 'boxed', 'framed' ) ) ) {
		return true;
	}

	return false;
}

/**
 * Return true if option `Header Background Transparent` is not enable. Otherwise - return false.
 *
 * @param  object $control Parent control.
 * @return bool
 */
function storycle_is_not_header_bg_transparent( $control ) {
	return storycle_is_not_setting( $control, 'header_bg_transparent', true );
}

// Change native customizer control (based on WordPress core).
add_action( 'customize_register', 'storycle_customizer_change_core_controls', 20 );

// Bind JS handlers to instantly live-preview changes.
add_action( 'customize_preview_init', 'storycle_customize_preview_js' );

/**
 * Change native customize control (based on WordPress core).
 *
 * @since 1.0.0
 * @param  object $wp_customize Object wp_customize.
 * @return void
 */
function storycle_customizer_change_core_controls( $wp_customize ) {
	$wp_customize->get_control( 'custom_logo' )->section       = 'storycle_logo_favicon';
	$wp_customize->get_control( 'site_icon' )->section         = 'storycle_logo_favicon';
	$wp_customize->get_section( 'background_image' )->priority = 45;
	$wp_customize->get_control( 'background_color' )->label    = esc_html__( 'Body Background Color', 'storycle' );

	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function storycle_customize_preview_js() {
	wp_enqueue_script( 'storycle-customize-preview', get_parent_theme_file_uri( 'assets/js/customize-preview.js' ), array( 'customize-preview' ), STORYCLE_THEME_VERSION, true );
}

// Typography utility function
/**
 * Get font styles
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_font_styles() {
	return apply_filters( 'storycle_get_font_styles', array(
		'normal'  => esc_html__( 'Normal', 'storycle' ),
		'italic'  => esc_html__( 'Italic', 'storycle' ),
		'oblique' => esc_html__( 'Oblique', 'storycle' ),
		'inherit' => esc_html__( 'Inherit', 'storycle' ),
	) );
}

/**
 * Get character sets
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_character_sets() {
	return apply_filters( 'storycle_get_character_sets', array(
		'latin'        => esc_html__( 'Latin', 'storycle' ),
		'greek'        => esc_html__( 'Greek', 'storycle' ),
		'greek-ext'    => esc_html__( 'Greek Extended', 'storycle' ),
		'vietnamese'   => esc_html__( 'Vietnamese', 'storycle' ),
		'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'storycle' ),
		'latin-ext'    => esc_html__( 'Latin Extended', 'storycle' ),
		'cyrillic'     => esc_html__( 'Cyrillic', 'storycle' ),
	) );
}

/**
 * Get text aligns
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_text_aligns() {
	return apply_filters( 'storycle_get_text_aligns', array(
		'inherit' => esc_html__( 'Inherit', 'storycle' ),
		'center'  => esc_html__( 'Center', 'storycle' ),
		'justify' => esc_html__( 'Justify', 'storycle' ),
		'left'    => esc_html__( 'Left', 'storycle' ),
		'right'   => esc_html__( 'Right', 'storycle' ),
	) );
}

/**
 * Get font weights
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_font_weight() {
	return apply_filters( 'storycle_get_font_weight', array(
		'100' => '100',
		'200' => '200',
		'300' => '300',
		'400' => '400',
		'500' => '500',
		'600' => '600',
		'700' => '700',
		'800' => '800',
		'900' => '900',
	) );
}

/**
 * Get tesx transform.
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_text_transform() {
	return apply_filters( 'storycle_get_text_transform', array(
		'none'       => esc_html__( 'None ', 'storycle' ),
		'uppercase'  => esc_html__( 'Uppercase ', 'storycle' ),
		'lowercase'  => esc_html__( 'Lowercase', 'storycle' ),
		'capitalize' => esc_html__( 'Capitalize', 'storycle' ),
	) );
}

// Background utility function
/**
 * Get background position
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_bg_position() {
	return apply_filters( 'storycle_get_bg_position', array(
		'top-left'      => esc_html__( 'Top Left', 'storycle' ),
		'top-center'    => esc_html__( 'Top Center', 'storycle' ),
		'top-right'     => esc_html__( 'Top Right', 'storycle' ),
		'center-left'   => esc_html__( 'Middle Left', 'storycle' ),
		'center'        => esc_html__( 'Middle Center', 'storycle' ),
		'center-right'  => esc_html__( 'Middle Right', 'storycle' ),
		'bottom-left'   => esc_html__( 'Bottom Left', 'storycle' ),
		'bottom-center' => esc_html__( 'Bottom Center', 'storycle' ),
		'bottom-right'  => esc_html__( 'Bottom Right', 'storycle' ),
	) );
}

/**
 * Get background size
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_bg_size() {
	return apply_filters( 'storycle_get_bg_size', array(
		'auto'    => esc_html__( 'Auto', 'storycle' ),
		'cover'   => esc_html__( 'Cover', 'storycle' ),
		'contain' => esc_html__( 'Contain', 'storycle' ),
	) );
}

/**
 * Get background repeat
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_bg_repeat() {
	return apply_filters( 'storycle_get_bg_repeat', array(
		'no-repeat' => esc_html__( 'No Repeat', 'storycle' ),
		'repeat'    => esc_html__( 'Tile', 'storycle' ),
		'repeat-x'  => esc_html__( 'Tile Horizontally', 'storycle' ),
		'repeat-y'  => esc_html__( 'Tile Vertically', 'storycle' ),
	) );
}

/**
 * Get background attachment
 *
 * @since 1.0.0
 * @return array
 */
function storycle_get_bg_attachment() {
	return apply_filters( 'storycle_get_bg_attachment', array(
		'scroll' => esc_html__( 'Scroll', 'storycle' ),
		'fixed'  => esc_html__( 'Fixed', 'storycle' ),
	) );
}

/**
 * Return array of arguments for dynamic CSS module
 *
 * @return array
 */
function storycle_get_dynamic_css_options() {
	return apply_filters( 'storycle_get_dynamic_css_options', array(
		'prefix'        => 'storycle',
		'type'          => 'theme_mod',
		'parent_handle' => 'storycle-theme-style',
		'single'        => true,
		'css_files'     => array(
			storycle_get_locate_template( 'assets/css/dynamic/dynamic.css' ),

			storycle_get_locate_template( 'assets/css/dynamic/site/elements.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/header.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/forms.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/menus.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/post.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/navigation.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/footer.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/misc.css' ),
			storycle_get_locate_template( 'assets/css/dynamic/site/buttons.css' ),

			storycle_get_locate_template( 'assets/css/dynamic/widgets/widget-default.css' ),
		),
		'options' => array(
			'header_logo_font_style',
			'header_logo_font_weight',
			'header_logo_font_size',
			'header_logo_line_height',
			'header_logo_font_family',
			'header_logo_letter_spacing',
			'header_logo_text_transform',

			'body_font_style',
			'body_font_weight',
			'body_font_size',
			'body_line_height',
			'body_font_family',
			'body_letter_spacing',
			'body_text_align',
			'body_text_transform',

			'h1_font_style',
			'h1_font_weight',
			'h1_font_size',
			'h1_line_height',
			'h1_font_family',
			'h1_letter_spacing',
			'h1_text_align',
			'h1_text_transform',

			'h2_font_style',
			'h2_font_weight',
			'h2_font_size',
			'h2_line_height',
			'h2_font_family',
			'h2_letter_spacing',
			'h2_text_align',
			'h2_text_transform',

			'h3_font_style',
			'h3_font_weight',
			'h3_font_size',
			'h3_line_height',
			'h3_font_family',
			'h3_letter_spacing',
			'h3_text_align',
			'h3_text_transform',

			'h4_font_style',
			'h4_font_weight',
			'h4_font_size',
			'h4_line_height',
			'h4_font_family',
			'h4_letter_spacing',
			'h4_text_align',
			'h4_text_transform',

			'h5_font_style',
			'h5_font_weight',
			'h5_font_size',
			'h5_line_height',
			'h5_font_family',
			'h5_letter_spacing',
			'h5_text_align',
			'h5_text_transform',

			'h6_font_style',
			'h6_font_weight',
			'h6_font_size',
			'h6_line_height',
			'h6_font_family',
			'h6_letter_spacing',
			'h6_text_align',
			'h6_text_transform',

			'breadcrumbs_font_style',
			'breadcrumbs_font_weight',
			'breadcrumbs_font_size',
			'breadcrumbs_line_height',
			'breadcrumbs_font_family',
			'breadcrumbs_letter_spacing',
			'breadcrumbs_text_transform',

			'custom_font_style',
			'custom_font_weight',
			'custom_font_family',
			'custom_letter_spacing',

			'regular_accent_color_1',
			'regular_accent_color_2',
			'regular_text_color',
			'regular_link_color',
			'regular_link_hover_color',
			'regular_h1_color',
			'regular_h2_color',
			'regular_h3_color',
			'regular_h4_color',
			'regular_h5_color',
			'regular_h6_color',

			'light_color',
			'grey_color_1',
			'dark_color',

			'header_bg_color',
			'header_bg_image',
			'header_bg_repeat',
			'header_bg_position',
			'header_bg_attachment',
			'header_bg_size',

			'page_preloader_bg',

			'page_boxed_width',
			'page_boxed_bg_color',

			'container_width',

			'footer_bg',
			'footer_text_color',

			'onsale_badge_bg',
			'featured_badge_bg',
			'new_badge_bg',
		),
	) );
}

// Add dynamic css plugins files.
add_filter( 'storycle_get_dynamic_css_options', 'storycle_add_dynamic_css_plugins_files' );

/**
 * Add third party plugins dynamic css files.
 *
 * @param array $args Dynamic css arguments.
 *
 * @return array
 */
function storycle_add_dynamic_css_plugins_files( $args= array() ) {

	$plugins_config = array(
		'woo' => array(
			'dynamic_css_file' => 'assets/css/dynamic/dynamic-woo.css',
			'conditional'      => array(
				'cb'  => 'class_exists',
				'arg' => 'WooCommerce',
			),
		),
		'elementor' => array(
			'dynamic_css_file' => 'assets/css/dynamic/plugins/elementor.css',
			'conditional'      => array(
				'cb'  => 'class_exists',
				'arg' => 'Elementor\Plugin',
			),
		),
		'cherry-trending-posts' => array(
			'dynamic_css_file' => 'assets/css/dynamic/plugins/cherry-trending-posts.css',
			'conditional'      => array(
				'cb'  => 'class_exists',
				'arg' => 'Cherry_Trending_Posts',
			),
		),
		'cherry-socialize' => array(
			'dynamic_css_file' => 'assets/css/dynamic/plugins/cherry-socialize.css',
			'conditional'      => array(
				'cb'  => 'class_exists',
				'arg' => 'Cherry_Socialize',
			),
		),
		'cherry-popups' => array(
			'dynamic_css_file' => 'assets/css/dynamic/plugins/cherry-popups.css',
			'conditional'      => array(
				'cb'  => 'class_exists',
				'arg' => 'Cherry_Popups',
			),
		),
	);

	foreach ( $plugins_config as $plugin ) {

		if ( is_callable( $plugin['conditional']['cb'] )
		     && true === call_user_func( $plugin['conditional']['cb'], $plugin['conditional']['arg'] )
		) {
			$args['css_files'][] = storycle_get_locate_template( $plugin['dynamic_css_file'] );
		}
	}

	return $args;
}

/**
 * Return array of arguments for Google Font loader module.
 *
 * @since  1.0.0
 * @return array
 */
function storycle_get_fonts_options() {
	return apply_filters( 'storycle_get_fonts_options', array(
		'prefix'  => 'storycle',
		'type'    => 'theme_mod',
		'single'  => true,
		'options' => array(
			'body' => array(
				'family'  => 'body_font_family',
				'style'   => 'body_font_style',
				'weight'  => 'body_font_weight',
				'charset' => 'body_character_set',
			),
			'body_italic' => array(
				'family'  => 'body_font_family',
				'style'   => 'body_font_style_italic',
				'weight'  => 'body_font_weight',
				'charset' => 'body_character_set',
			),
			'body_bold' => array(
				'family'  => 'body_font_family',
				'style'   => 'body_font_style',
				'weight'  => 'body_font_weight_bold',
				'charset' => 'body_character_set',
			),
			'body_bold_italic' => array(
				'family'  => 'body_font_family',
				'style'   => 'body_font_style_italic',
				'weight'  => 'body_font_weight_bold',
				'charset' => 'body_character_set',
			),
			'h1' => array(
				'family'  => 'h1_font_family',
				'style'   => 'h1_font_style',
				'weight'  => 'h1_font_weight',
				'charset' => 'h1_character_set',
			),
			'h2' => array(
				'family'  => 'h2_font_family',
				'style'   => 'h2_font_style',
				'weight'  => 'h2_font_weight',
				'charset' => 'h2_character_set',
			),
			'h3' => array(
				'family'  => 'h3_font_family',
				'style'   => 'h3_font_style',
				'weight'  => 'h3_font_weight',
				'charset' => 'h3_character_set',
			),
			'h4' => array(
				'family'  => 'h4_font_family',
				'style'   => 'h4_font_style',
				'weight'  => 'h4_font_weight',
				'charset' => 'h4_character_set',
			),
			'h5' => array(
				'family'  => 'h5_font_family',
				'style'   => 'h5_font_style',
				'weight'  => 'h5_font_weight',
				'charset' => 'h5_character_set',
			),
			'h6' => array(
				'family'  => 'h6_font_family',
				'style'   => 'h6_font_style',
				'weight'  => 'h6_font_weight',
				'charset' => 'h6_character_set',
			),
			'header_logo' => array(
				'family'  => 'header_logo_font_family',
				'style'   => 'header_logo_font_style',
				'weight'  => 'header_logo_font_weight',
				'charset' => 'header_logo_character_set',
			),
			'custom' => array(
				'family'  => 'custom_font_family',
				'style'   => 'custom_font_style',
				'weight'  => 'custom_font_weight',
				'charset' => 'custom_character_set',
			),
			'breadcrumbs' => array(
				'family'  => 'breadcrumbs_font_family',
				'style'   => 'breadcrumbs_font_style',
				'weight'  => 'breadcrumbs_font_weight',
				'charset' => 'breadcrumbs_character_set',
			),
		),
	) );
}

/**
 * Get blog layouts.
 *
 * @since  1.0.0
 * @return array
 */
function storycle_get_blog_layouts() {
	return apply_filters( 'storycle_get_blog_layouts', array(
		'default'           => esc_html__( 'Listing 1', 'storycle' ),
		'default-small-img' => esc_html__( 'Listing 2', 'storycle' ),
		'grid'              => esc_html__( 'Grid 1', 'storycle' ),
		'grid-2'            => esc_html__( 'Grid 2', 'storycle' ),
		'masonry'           => esc_html__( 'Masonry', 'storycle' ),
		'vertical-justify'  => esc_html__( 'Vertical Justify', 'storycle' ),
		'timeline'          => esc_html__( 'Timeline', 'storycle' ),
	) );
}

/**
 * Get default footer copyright.
 *
 * @since  1.0.0
 * @return string
 */
function storycle_get_default_footer_copyright() {
	return esc_html__( '&copy; Copyright %%year%%. All rights reserved.', 'storycle' );
}

/**
 * Get FontAwesome icons set
 *
 * @return array
 */
function storycle_get_icons_set() {

	static $font_icons;

	if ( ! $font_icons ) {

		ob_start();

		include get_parent_theme_file_path( 'assets/js/icons.json' );
		$json = ob_get_clean();

		$font_icons = array();
		$icons      = json_decode( $json, true );

		foreach ( $icons['icons'] as $icon ) {
			$font_icons[] = $icon['id'];
		}
	}

	return $font_icons;
}

function storycle_get_fa_icons_data() {
	return apply_filters( 'storycle_get_fa_icons_data', array(
		'icon_set'    => 'storycleFontAwesome',
		'icon_css'    => get_parent_theme_file_uri( 'assets/css/font-awesome.min.css' ),
		'icon_base'   => 'fa',
		'icon_prefix' => 'fa-',
		'icons'       => storycle_get_icons_set(),
	) );
}

/**
 * Get mdi icons set.
 *
 * @return array
 */
function storycle_get_mdi_icons_set() {

	static $mdi_icons;

	if ( ! $mdi_icons ) {
		ob_start();

		include get_parent_theme_file_path( 'assets/css/materialdesignicons.min.css' );

		$result = ob_get_clean();

		preg_match_all( '/\.([-_a-zA-Z0-9]+):before[, {]content:/', $result, $matches );

		if ( ! is_array( $matches ) || empty( $matches[1] ) ) {
			return;
		}

		$mdi_icons = $matches[1];
	}

	return $mdi_icons;
}

/**
 * Return sanitized theme mod value.
 *
 * @param  string       $mod               Mod name to get.
 * @param  bool         $use_default       Use or not default value.
 * @param  string|array $sanitize_callback Sanitize callback name.
 * @return mixed
 */
function storycle_get_mod( $mod = null, $use_default = true, $sanitize_callback = null ) {

	if ( ! $mod ) {
		return false;
	}

	$default = false;

	if ( true === $use_default ) {
		$default = storycle_theme()->customizer->get_default( $mod );
	}

	$value = get_theme_mod( $mod, $default );

	if ( is_callable( $sanitize_callback ) ) {
		return call_user_func( $sanitize_callback, $value );
	} else {
		return $value;
	}
}
